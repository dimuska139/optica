<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use \app\models\Goods;
use \app\models\Colors;
use \app\models\Frames;
use \app\models\Brands;
use \app\models\Forms;
use \app\models\Photos;
use \app\models\Materials;
use \app\models\FramesTypes;
use \app\models\FramesSizes;
use \app\models\Manufacturers;
use yii\helpers\ArrayHelper;


class FramesController extends Controller
{
    // Заполнение тестовыми данными
    public function actionFill(){
        die;
        $brands = array_keys(ArrayHelper::map(Brands::find()->all(), 'id', 'name'));
        $forms = array_keys(ArrayHelper::map(Forms::find()->all(), 'id', 'name'));
        $frame_types = array_keys(ArrayHelper::map(FramesTypes::find()->all(), 'id', 'name'));
        $frame_sizes = array_keys(ArrayHelper::map(FramesSizes::find()->all(), 'id', 'size'));
        $materials = array_keys(ArrayHelper::map(Materials::find()->all(), 'id', 'name'));
        $colors = array_keys(ArrayHelper::map(Colors::find()->all(), 'id', 'name'));
        $manufacturers = array_keys(ArrayHelper::map(Manufacturers::find()->all(), 'id', 'name'));
        for ($i=0;$i<500;$i++){
            $brand_id = $brands[array_rand($brands)];
            $good = new Goods();
            $good->name = "Оправа ".$i;
            $good->description = "Оправы очков можно разделить на три типа: ободковые, полуободковые (на леске) и "
                    . "безободковые. Если вы первый раз хотите купить оправу, рекомендуем померить все варианты, "
                    . "чтобы определить какая форма очков наиболее комфортна для вас. В нашем каталоге вы найдете "
                    . "женские и мужские оправы оригинальных и классических форм, цветов, изготовленных из различных материалов, "
                    . "в том числе из сплавов титана, ацетатного пластика и "
                    . "позолоты. Как подобрать оправу по форме лица?"
                    . " Определите тип вашего лица: овальное, круглое, квадратное, сердцевидное, трапециевидное, треугольное, "
                    . "прямоугольное, ромбовидное. Людям с овальным лицом повезло, т.к. такая форма считается наиболее пропорциональной, "
                    . "вам подойдет большая часть оправ. Экспериментируйте, пока не найдете то, что добавит изюминки в ваш образ. "
                    . "Круглое лицо обладает примерно одной шириной и длиной, при этом обладает мягкой формой. Не рекомендуется использовать "
                    . "массивные, круглые и грубые оправы, лучше визуально сузить лицо прямоугольной вытянутой формой. Она откорректирует "
                    . "округлость и придаст выразительности глазам. Квадратное лицо обладает широкими скулами и квадратной линией челюсти. "
                    . "Выбирайте оправы круглой, овальной формы, а так же авиаторы и кошачий глаз. Такие оправы способны смягчить линию подбородка "
                    . "и перенести вес на скулы. Широкое сверху и узкое снизу лицо сердцевидной формы откорректируют оправы в форме бабочка и кошачий "
                    . "глаз. Если ваше лицо сердцевидной формы, обратите внимание на безободковые оправы. Трапециевидное лицо украсит крупная, "
                    . "широкая оправа, а так же форма кошачий глаз. Не подойдут узкие и маленькие оправы для очков, а так же резкие квадратные "
                    . "и прямоугольные формы. При форме лица, напоминающей перевернутый треугольник, необходимо откорректировать ширину щек "
                    . "и подбородка. Для этого лучшим выбором будет оправа с широкой верхней частью, например, кошачий глаз. При вытянутом"
                    . "прямоугольном лице можно смело эксперементировать с крупными оправами, практически не ограничивая себя в формах!"
                    . " Достаточно редкий ромбовидный тип лица можно сузить в районе скул круглой оправой, авиаторами и формой \"кошачий глаз\".";
            $good->logo = "frame.jpg";
            $good->price = rand(3000,6000);
            $good->good_type = 2;
            $good->brand_id = $brand_id;
            $good->views_amount = rand(0,1000);
            $good->published = 1;
            $good->owner_id = 1;
            $good->ip = ip2long($_SERVER['REMOTE_ADDR']);
            $good->sku = (string)rand(10000,99999);
            $good->new = rand(0,1);
            $good->amount = rand(0,100);
            $manufacturer_id = $manufacturers[array_rand($manufacturers)];
            $good->manufacturer_id = $manufacturer_id;
            if (!$good->save()){
                var_dump($good->errors);
                die;
            }
            $frame = new Frames();
            $subtypes = ['male','female','child'];
            $subtype = $subtypes[array_rand($subtypes)];
            $frame->subtype = $subtype; // male - мужские, female - женские, child - детские
            $frame->form_id = $forms[array_rand($forms)];
            $frame->frame_type_id = $frame_types[array_rand($frame_types)];
            $frame->frame_material_id = $materials[array_rand($materials)];
            $frame->frame_color_id = $colors[array_rand($colors)];
            $frame->frameID = $good->id;
            
            if (!$frame->save()){
                var_dump($frame->errors);
                die;
            }
        }
        die;
        
    }

    public function actionIndex()
    {   
        $sorters_array = array();
        $sorters_array['popularity'] = 'Популярность';
        $sorters_array['brend_az'] = 'Бренд (A-Z)';
        $sorters_array['brend_za'] = 'Бренд (Z-A)';
        $sorters_array['price_inc'] = 'Цена (по возрастанию)';
        $sorters_array['price_dec'] = 'Цена (по убыванию)';
        

        Yii::$app->view->params['breadcrumbs'][] = [
            'label' => 'ОПРАВЫ',
            'template' => '<span>{link}</span>'
        ];
        $this->view->registerCssFile('resources/css/frames.css');
        $this->view->registerJsFile('resources/js/jquery.json.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/optica_paginator.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/frames.js',['depends'=>'yii\web\JqueryAsset']);
        
        
        $brands = Brands::find()->orderBy(['name'=> SORT_ASC])->all();
        $forms = Forms::find()->orderBy(['name'=> SORT_ASC])->all();
        $frame_types = FramesTypes::find()->orderBy(['name'=> SORT_ASC])->all();
        
        $frame_materials = Materials::find()
                            ->where(['id' => ArrayHelper::getColumn(ArrayHelper::toArray(Frames::find()
                                                ->select('frame_material_id')
                                                ->distinct()
                                                ->orderBy(['frame_material_id'=> SORT_ASC])
                                                ->all(), 'id', 'frame_material_id'),'frame_material_id')])
                            ->orderBy(['name'=> SORT_ASC])
                            ->all();
        // получение всех цветов оправ, с которыми существуют очки 
        $frame_colors = Colors::find()
                            ->where(['id' => ArrayHelper::getColumn(ArrayHelper::toArray(Frames::find()
                                                ->select('frame_color_id')
                                                ->distinct()
                                                ->orderBy(['frame_color_id'=> SORT_ASC])
                                                ->all(), 'id', 'frame_color_id'),'frame_color_id')])
                            ->orderBy(['name'=> SORT_ASC])
                            ->all();
  /*      // получение всех цветов оправ, с которыми существуют очки 
        $frame_sizes = FramesSizes::find()
                            ->where(['id' => ArrayHelper::getColumn(ArrayHelper::toArray(FramesAmount::find()
                                                ->select('frame_size_id')
                                                ->distinct()
                                                ->orderBy(['frame_size_id'=> SORT_ASC])
                                                ->all(), 'id', 'frame_size_id'),'frame_size_id')])
                            ->orderBy(['size'=> SORT_ASC])
                            ->all();
        */
        
        return $this->render('index', [
            'sorter_by_array' => $sorters_array,
            'brands' => $brands,
            'forms' => $forms,
            'frame_types' => $frame_types,
            'frame_materials' => $frame_materials,
       //     'frame_sizes' => $frame_sizes,
            'frame_colors' => $frame_colors
        ]);
    }

    // Получение списка оправ
    public function actionGetlist(){
        $frames = Goods::find()
                ->leftJoin('brands b', 'brand_id=b.id')
                ->leftJoin('frames frm', 'goods.id=frm.goodID');
        $where = array('published' => 1);
        $where['good_type'] = 2;
        $frames->where($where);
        
        if (!empty($_REQUEST['new']))
            $frames->andWhere('`new`=1');
        
        // цена от
        if (!empty($_REQUEST['price_from']))
            $frames->andWhere('`price`>:price_from', [':price_from'=>$_REQUEST['price_from']]);
        
        // цена до
        if (!empty($_REQUEST['price_to']))
            $frames->andWhere('`price`<:price_to', [':price_to'=>$_REQUEST['price_to']]);
        
        // бренды
        if (!empty($_REQUEST['brands'])){
            $brands_ids = json_decode($_REQUEST['brands']);
            sort($brands_ids);
            $frames->andWhere(['brand_id' => $brands_ids]);
        }
        
        // формы
        if (!empty($_REQUEST['forms'])){
            $forms_ids = json_decode($_REQUEST['forms']);
            sort($forms_ids);
            $frames->andWhere(['frm.form_id' => $forms_ids]);
        }
        
        // типы оправ
        if (!empty($_REQUEST['frames'])){
            $frames_ids = json_decode($_REQUEST['frames']);
            sort($frames_ids);
            $frames->andWhere(['frm.frame_type_id' => $frames_ids]);
        }
        
        // материалы оправ
        if (!empty($_REQUEST['materials'])){
            $materials_ids = json_decode($_REQUEST['materials']);
            sort($materials_ids);
            $frames->andWhere(['frm.frame_material_id' => $materials_ids]);
        }
        
        // по полу
        if (!empty($_REQUEST['subtypes'])){
            $subtype_ids = json_decode($_REQUEST['subtypes']);
            sort($subtype_ids);
            $frames->andWhere(['frm.subtype' => $subtype_ids]);
        }
        
        
    /*    // по размеру
        if (!empty($_REQUEST['frame_size_id'])){
            $ids_arr = ArrayHelper::getColumn(ArrayHelper::toArray(FramesAmount::find()
                                                ->select('framesID')
                                                ->distinct()
                                                ->andWhere('`frame_size_id`=:frame_size_id', [':frame_size_id'=>$_REQUEST['frame_size_id']])
                                                ->all(), 'id', 'framesID'),'framesID');
            $frames->andWhere(['goods.id' => $ids_arr]);
        }*/
        
        
        // по цвету оправы
        if (!empty($_REQUEST['frame_color_id'])){
            $frames->andWhere(['frm.frame_color_id' => (int)$_REQUEST['frame_color_id']]);
        /*    $ids_arr = ArrayHelper::getColumn(ArrayHelper::toArray(Frames::find()
                                                ->select('goodID')
                                                ->distinct()
                                                ->andWhere('`frame_color_id`=:frame_color_id', [':frame_color_id'=>$_REQUEST['frame_color_id']])
                                                ->all(), 'id', 'goodID'),'goodID');
            $frames->andWhere(['goods.id' => $ids_arr]);*/
        }
        
        
        // сортировка
        if (empty($_REQUEST['sortby']) || strcmp($_REQUEST['sortby'],'popularity')==0)
            $frames->orderBy(['views_amount'=> SORT_DESC]);
        else {
            switch($_REQUEST['sortby']){
                case 'brend_az':
                    $frames->orderBy(['b.name'=> SORT_ASC]);
                    break;
                case 'brend_za':
                    $frames->orderBy(['b.name'=> SORT_DESC]);
                    break;
                case 'price_inc':
                    $frames->orderBy(['price'=> SORT_ASC]);
                    break;
                case 'price_dec':
                    $frames->orderBy(['price'=> SORT_DESC]);
                    break;
            }
        }
        $countQuery = clone $frames;
        $totalCount = $countQuery->count();
        $models = $frames->offset(($_REQUEST['page']-1)*30)
            ->limit(30)
            ->all();
        echo json_encode(array(
            'html'=>$this->renderAjax('list',array(
                    'frames' => $models  
                )
            ),
            'count' => $totalCount
        ));
    }
    
    // Карточка товара
    public function actionProduct(){
        if (empty($_GET['id']))
            throw new HttpException(404, 'Not Found');
        else {
            $product = Goods::find()
                ->where(['published' => 1,'good_type' => 2, 'goods.id' => (int)$_GET['id']])
                ->one();
            if (empty($product))
                throw new HttpException(404, 'Product Does Not Exist');
            $product->views_amount++; // Увеличение счетчика просмотров
            $product->save();
            
            $history_ids = Goods::getHistory(); // Получение списка просмотренных товаров
            $history = Goods::find()
                    ->where(['id' => $history_ids, 'published' => 1])
                    ->all();

            Goods::toHistory($_GET['id']); // Добавление в историю просмотренных товаров
            
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => "ОПРАВЫ",
                'url' => '/frames',
                'template' => '<span>{link}</span> - '
            ];
            
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => mb_strtoupper($product->name, "utf-8"),
                'template' => '<span>{link}</span>'
            ];
            // Получение фотографий товара
            $photos = Photos::find()
                    ->where(['good_id' => (int)$_GET['id']])
                    ->offset(0)
                    ->limit(4)
                    ->all();
            
            $this->view->registerCssFile('/resources/css/history.css');
            $this->view->registerCssFile('/resources/css/similar.css');
            $this->view->registerCssFile('/resources/css/frames_product.css');
            $this->view->registerJsFile('/resources/js/frames_product.js',['depends'=>'yii\web\JqueryAsset']);
            $product_subtype = "мужские";
            $show_popup = true;
            switch($product->frames->subtype){
                case 'female':
                    $product_subtype = "женские";
                    break;
                case 'child':
                    $product_subtype = "детские";
                    $show_popup = false;
                    break;
            }
            // Список похожих товаров
            $similar = Goods::find()
                ->where(['published' => 1,'good_type' => 2, 'goods.sku' => $product->sku])
                ->andWhere(['<>', 'id', $product->id])
                ->all();
            return $this->render('product', [
                'product' => $product,
                'photos' => $photos,
                'subtype' => $product_subtype,
                'similar' => $similar,
                'history' => $history,
                'show_popup' => $show_popup]
                /*'sorter_by_array' => $sorters_array,
                'brands' => $brands,
                'forms' => $forms,
                'frame_types' => $frame_types,
                'frame_materials' => $frame_materials,
                'lens_colors' => $lens_colors,
                'frame_colors' => $frame_colors
            ]*/);
        }
    }
}
