<?php

namespace app\controllers;

use Yii;
use \app\models\Goods;
use app\models\CartForm;

// Корзина
class CartController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $cart_arr = array();
        if (!empty($_COOKIE['cart'])){
            $cart_arr = json_decode($_COOKIE['cart'], true);
        }
        
        $cart_amount_arr = array();
        if (!empty($_COOKIE['cart_amount'])){
            $cart_amount_arr = json_decode($_COOKIE['cart_amount'], true);
        }
        
        $cart_form = new CartForm();
        if ($cart_form->load(Yii::$app->request->post()) && $cart_form->validate()) {
            var_dump($_POST);
            die;
        }
        
        
        
    //    if (count($cart_arr)>0){ // Если корзина не пуста
            $this->view->registerCssFile('resources/css/cart.css');
            $this->view->registerJsFile('resources/js/cart.js',['depends'=>'yii\web\JqueryAsset']);
            $cart_list = Goods::find()
                    ->where(['id' => $cart_arr, 'published' => 1])
                    ->all();
            
            // Правильным образом упорядочиваем массив с количеством каждого из товаров в корзине
            $amount_order = array();
            foreach ($cart_list as $number => $item){
                for ($i=0;$i<count($cart_arr);$i++){
                    if ($cart_arr[$i]==$item->id){
                        $amount_order[] = $cart_amount_arr[$i];
                        break;
                    }
                        
                }
            }
            $delivery_array = [
                'himself' => 'Самовывоз',
                'moscow' => 'Доставка по Москве в пределах МКАД',
                'russia' => 'Доставка товара по России'
            ];
            $cart_form->delivery = 'himself';
            $payment_array = [
                'online' => 'Оплата он-лайн',
                'moscow' => 'Оплата по Москве курьеру наличными',
                'courier' => 'Оплата курьеру наличными (ЕМС)',
                'himself' => 'Оплата наличными (самовывоз)'
            ];
            $cart_form->payment = 'online';
            return $this->render('index', [
                'cart_list' => $cart_list,
                'amount_order' => $amount_order,
                'cart_form' => $cart_form,
                'delivery_array' => $delivery_array,
                'payment_array' => $payment_array
            ]);
      /*  } else // Иначе выкидываем на главную
            $this->redirect('/', TRUE, 301);*/
    }


}
