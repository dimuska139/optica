<?php

namespace app\controllers;

use Yii;
use \app\models\Goods;
use \app\models\Colors;
use \app\models\Sunglasses;
use \app\models\Brands;
use \app\models\Forms;
use \app\models\Materials;
use \app\models\FramesTypes;
use \app\models\Manufacturers;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;

// Глобальный пооиск товаров по сайту
class SearchController extends \yii\web\Controller
{
    public function actionIndex()
    {
        $sorters_array = array();
        $sorters_array['popularity'] = 'Популярность';
        $sorters_array['brend_az'] = 'Бренд (A-Z)';
        $sorters_array['brend_za'] = 'Бренд (Z-A)';
        $sorters_array['price_inc'] = 'Цена (по возрастанию)';
        $sorters_array['price_dec'] = 'Цена (по убыванию)';
        

        Yii::$app->view->params['breadcrumbs'][] = [
            'label' => 'ПОИСК',
            'template' => '<span>{link}</span>'
        ];
        $this->view->registerCssFile('resources/css/search.css');
        $this->view->registerJsFile('resources/js/jquery.json.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/optica_paginator.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/search.js',['depends'=>'yii\web\JqueryAsset']);
        
        
        $brands = Brands::find()->orderBy(['name'=> SORT_ASC])->all();
        $forms = Forms::find()->orderBy(['name'=> SORT_ASC])->all();
        $frame_types = FramesTypes::find()->orderBy(['name'=> SORT_ASC])->all();
        $frame_materials = Materials::find()
                            ->where(['id' => ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('frame_material_id')
                                                ->distinct()
                                                ->orderBy(['frame_material_id'=> SORT_ASC])
                                                ->all(), 'id', 'frame_material_id'),'frame_material_id')])
                            ->orderBy(['name'=> SORT_ASC])
                            ->all();
        
        // получение всех цветов оправ, с которыми существуют очки 
        $frame_colors = Colors::find()
                            ->where(['id' => ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('frame_color_id')
                                                ->distinct()
                                                ->orderBy(['frame_color_id'=> SORT_ASC])
                                                ->all(), 'id', 'frame_color_id'),'frame_color_id')])
                            ->orderBy(['name'=> SORT_ASC])
                            ->all();
        // получение всех цветов линз, с которыми существуют очки 
        $lens_colors = Colors::find()
                            ->where(['id' => ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('lens_color_id')
                                                ->distinct()
                                                ->orderBy(['lens_color_id'=> SORT_ASC])
                                                ->all(), 'id', 'lens_color_id'),'lens_color_id')])
                            ->orderBy(['name'=> SORT_ASC])
                            ->all();
        
        
        
        return $this->render('index', [
            'sorter_by_array' => $sorters_array,
            'brands' => $brands,
            'forms' => $forms,
            'frame_types' => $frame_types,
            'frame_materials' => $frame_materials,
            'lens_colors' => $lens_colors,
            'frame_colors' => $frame_colors
        ]);
    }
    
    public function actionGetlist(){
        $goods = Goods::find()
                ->leftJoin('sunglasses sng', 'goods.id=sng.goodID')
                ->leftJoin('related rltd', 'goods.id=rltd.goodID')
                ->leftJoin('lenses l', 'goods.id=l.goodID')
                ->leftJoin('frames frm', 'goods.id=frm.goodID')
                ->leftJoin('brands b', 'brand_id=b.id')
                ->leftJoin('manufacturers mnfs', 'manufacturer_id=mnfs.id');
        $where = array('published' => 1);

        $goods->where($where);
        
        // поисковый запрос
        if (!empty($_REQUEST['search'])){
            $goods->andWhere(['like', 'goods.name', '%'.$_REQUEST['search'].'%', false]);
            $goods->orWhere(['like', 'goods.description', '%'.$_REQUEST['search'].'%', false]);
            $goods->orWhere(['like', 'b.name', '%'.$_REQUEST['search'].'%', false]);
            $goods->orWhere(['like', 'mnfs.name', '%'.$_REQUEST['search'].'%', false]);
        }
        
        // типы товаров
        if (!empty($_REQUEST['good_types'])){
            $good_types = json_decode($_REQUEST['good_types']);
            sort($good_types);
            $goods->andWhere(['good_type' => $good_types]);
        }
        
        // цена от
        if (!empty($_REQUEST['price_from']))
            $goods->andWhere('`price`>:price_from', [':price_from'=>$_REQUEST['price_from']]);
        
        // цена до
        if (!empty($_REQUEST['price_to']))
            $goods->andWhere('`price`<:price_to', [':price_to'=>$_REQUEST['price_to']]);
        
        // бренды
        if (!empty($_REQUEST['brands'])){
            $brands_ids = json_decode($_REQUEST['brands']);
            sort($brands_ids);
            $goods->andWhere(['brand_id' => $brands_ids]);
        }
        
        // формы
        if (!empty($_REQUEST['forms'])){
            $forms_ids = json_decode($_REQUEST['forms']);
            sort($forms_ids);
            $goods->andWhere(['sng.form_id' => $forms_ids]);
        }
        
        // типы оправ
        if (!empty($_REQUEST['frames'])){
            $frames_ids = json_decode($_REQUEST['frames']);
            sort($frames_ids);
            $goods->andWhere(['sng.frame_type_id' => $frames_ids]);
        }
        
        // материалы оправ
        if (!empty($_REQUEST['materials'])){
            $materials_ids = json_decode($_REQUEST['materials']);
            sort($materials_ids);
            $goods->andWhere(['sng.frame_material_id' => $materials_ids]);
        }
        
        // по полу
        if (!empty($_REQUEST['subtypes'])){
            $subtype_ids = json_decode($_REQUEST['subtypes']);
            sort($subtype_ids);
            $goods->andWhere(['sng.subtype' => $subtype_ids]);
        }
        
        
        // по цвету линз
        if (!empty($_REQUEST['lens_color_id'])){
            $goods->andWhere(['sng.lens_color_id' => (int)$_REQUEST['lens_color_id']]);
        /*    $ids_arr = ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('goodID')
                                                ->distinct()
                                                ->andWhere('`lens_color_id`=:lens_color_id', [':lens_color_id'=>$_REQUEST['lens_color_id']])
                                                ->all(), 'id', 'goodID'),'goodID');
            $sunglasses->andWhere(['goods.id' => $ids_arr]);*/
        }
        
        // по цвету оправы
        if (!empty($_REQUEST['frame_color_id'])){
            $goods->andWhere(['sng.frame_color_id' => (int)$_REQUEST['frame_color_id']]);
       /*     $ids_arr = ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('goodID')
                                                ->distinct()
                                                ->andWhere('`frame_color_id`=:frame_color_id', [':frame_color_id'=>$_REQUEST['frame_color_id']])
                                                ->all(), 'id', 'goodID'),'goodID');
            $sunglasses->andWhere(['goods.id' => $ids_arr]);*/
        }
        
        
        // сортировка
        if (empty($_REQUEST['sortby']) || strcmp($_REQUEST['sortby'],'popularity')==0)
            $goods->orderBy(['views_amount'=> SORT_DESC]);
        else {
            switch($_REQUEST['sortby']){
                case 'brend_az':
                    $goods->orderBy(['b.name'=> SORT_ASC]);
                    break;
                case 'brend_za':
                    $goods->orderBy(['b.name'=> SORT_DESC]);
                    break;
                case 'price_inc':
                    $goods->orderBy(['price'=> SORT_ASC]);
                    break;
                case 'price_dec':
                    $goods->orderBy(['price'=> SORT_DESC]);
                    break;
            }
        }
        $countQuery = clone $goods;
        $totalCount = $countQuery->count();
        $models = $goods->offset(($_REQUEST['page']-1)*30)
            ->limit(30)
            ->all();
        echo json_encode(array(
            'html'=>$this->renderAjax('list',array(
                    'goods' => $models  
                )
            ),
            'count' => $totalCount
        ));
    }
}
