<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Users;


// Онлайн-консультант
class ConsultantController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    // Обновление времени последнего присутствия консультанта на сайте 
    public function actionOnline(){
        $consultant = Users::findOne(1);
        if (!empty($consultant)){
            $consultant->lastonline_time = date("Y-m-d H:i:s");
            $consultant->save();
        //    print_r($consultant->errors);
        }
    }
    
    // Онлайн или офлайн консультант?
    public function actionGetstatus(){
        $consultant = Users::findOne(1);
        $response = array('online'=>1);
        if (!empty($consultant)){
            $now = strtotime(date("Y-m-d H:i:s"));
            $last = strtotime($consultant->lastonline_time);
            $diff_sec = $now - $last;
            if ($diff_sec>5) // Если больше 5 сек не менялось время, то офлайн
                $response['online'] = 0;
        }
        echo json_encode($response);
    }
    
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
}
