<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use \app\models\Goods;
use \app\models\Lenses;
use \app\models\Brands;
use \app\models\Colors;
use \app\models\Photos;
use \app\models\Materials;
use \app\models\Manufacturers;
use \app\models\LensesWearingTime;
use \app\models\LensesWearingMode;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;


class LensesController extends Controller
{
    // Заполнение тестовыми данными
    public function actionFill(){
        die;
        $brands = array_keys(ArrayHelper::map(Brands::find()->all(), 'id', 'name'));
        $manufacturers = array_keys(ArrayHelper::map(Manufacturers::find()->all(), 'id', 'name'));
        $wearing_tymes = array_keys(ArrayHelper::map(LensesWearingTime::find()->all(), 'id', 'time'));
        $wearing_modes = array_keys(ArrayHelper::map(LensesWearingMode::find()->all(), 'id', 'mode'));
        $materials = array_keys(ArrayHelper::map(Materials::find()->all(), 'id', 'name'));
        for ($i=0;$i<500;$i++){
            $brand_id = $brands[array_rand($brands)];
            $good = new Goods();
            $subtypes = ['spherical','astigmatic'];
            $subtype = $subtypes[array_rand($subtypes)];
            $name = "Сферические линзы ";
            switch($subtype){
                case 'spherical':
                    $name = "Сферические линзы ";
                    break;
                case 'astigmatic':
                    $name = "Астигматические линзы ";
                    break;
            }
            $good->name = $name.$i;
            switch($subtype){
                case 'spherical':
                    $good->description = "Супер-тонкие и легкие сферические полимерные очковые линзы, с высоким коэффициентом преломления 1,67. Все линзы с упрочняющим покрытием, нанесен просветляющий слой, грязе- и водоотталкивающее покрытие (16 слоев). Эти линзы для очков могут быть окрашены в 240 различных цветов, светопропускание 98,5%. 100% защита от ультрафиолета, высокий комфорт при ношении очков, четкое видение во всех областях зрения, элегантные, легкие. Эластичный, практически не бьющийся материал.";
                    break;
                case 'astigmatic':
                    $good->description = "Астигматизм – это нарушение функционирования органов зрения различной степени, при котором человек видит предметы искаженными или размытыми вне зависимости от расстояния до них. Это состояние может развиться в любом возрасте. Часто сопровождают астигматизм близорукость или дальнозоркость.";
                    break;
            }
            $logo = "spherical.jpg";
            switch($subtype){
                case 'spherical':
                    $logo = "spherical.jpg";
                    break;
                case 'astigmatic':
                    $logo = "astigmatic.jpg";
                    break;
            }
            $good->logo = $logo;
            $good->price = rand(100,3000);
            $good->good_type = 3;
            $good->brand_id = $brand_id;
            $good->views_amount = rand(0,1000);
            $good->published = 1;
            $good->owner_id = 1;
            $good->ip = ip2long($_SERVER['REMOTE_ADDR']);
            $good->sku = (string)rand(10000,99999);
            $good->new = rand(0,1);
            $manufacturer_id = $manufacturers[array_rand($manufacturers)];
            $good->manufacturer_id = $manufacturer_id;
            $good->rand_sort = rand(1,9999999);
            $good->amount = rand(0,600);
            if (!$good->save()){
                var_dump($good->errors);
                die;
            }
            
            $photo = new Photos();
            $photo->photo_name = $logo;
            $photo->good_id = $good->id;
            if (!$photo->save()){
                var_dump($photo->errors);
                die;
            }
            
            $rel = new Lenses();
            $rel->goodID = $good->id;
            $rel->subtype = $subtype;
            $rel->wearing_time_id = $wearing_tymes[array_rand($wearing_tymes)];
            $rel->wearing_mode_id = $wearing_modes[array_rand($wearing_modes)];
            $rel->blisters_amount = rand (20,50);
            $rel->moisture_content = rand(20,90);
            $rel->material_id = $materials[array_rand($materials)];
            $rel->transmittance_of_oxygen = rand(10,90);
            if (!$rel->save()){
                var_dump($rel->errors);
                die;
            }
        }
        die;
    }
    
    public function actionIndex()
    {   
        $sorters_array = array();
        $sorters_array['popularity'] = 'Популярность';
        $sorters_array['brend_az'] = 'Бренд (A-Z)';
        $sorters_array['brend_za'] = 'Бренд (Z-A)';
        $sorters_array['price_inc'] = 'Цена (по возрастанию)';
        $sorters_array['price_dec'] = 'Цена (по убыванию)';
        Yii::$app->view->params['breadcrumbs'][] = [
            'label' => 'КОНТАКТНЫЕ ЛИНЗЫ',
            'template' => '<span>{link}</span>'
        ];
        $this->view->registerCssFile('resources/css/lenses.css');
        $this->view->registerJsFile('resources/js/jquery.json.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/optica_paginator.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/lenses.js',['depends'=>'yii\web\JqueryAsset']);
        $brands = Brands::find()->orderBy(['name'=> SORT_ASC])->all();
        $manufacturers = Manufacturers::find()->orderBy(['name'=> SORT_ASC])->all();
        $wearing_times = LensesWearingTime::find()->all();
        $wearing_modes = LensesWearingMode::find()->all();

        return $this->render('index', [
            'brands' => $brands,
            'manufacturers' => $manufacturers,
            'sorter_by_array' => $sorters_array,
            'wearing_time' => $wearing_times,
            'wearing_mode' => $wearing_modes
        ]);
    }
    
    // Получение списка
    public function actionGetlist(){
        $lenses = Goods::find()
           ->leftJoin('brands b', 'brand_id=b.id')
            ->leftJoin('lenses l', 'goods.id=l.goodID');
        $where = array('published' => 1);
        $where['good_type'] = 3;
        $lenses->where($where);

        if (!empty($_REQUEST['new']))
            $lenses->andWhere('`new`=1');
        
        // цена от
        if (!empty($_REQUEST['price_from']))
            $lenses->andWhere('`price`>:price_from', [':price_from'=>$_REQUEST['price_from']]);

        // цена до
        if (!empty($_REQUEST['price_to']))
            $lenses->andWhere('`price`<:price_to', [':price_to'=>$_REQUEST['price_to']]);

        // бренды
        if (!empty($_REQUEST['brands'])){
            $brands_ids = json_decode($_REQUEST['brands']);
            sort($brands_ids);
            $lenses->andWhere(['brand_id' => $brands_ids]);
        }
        
        // производители
        if (!empty($_REQUEST['manufacturers'])){
            $manufacturers_ids = json_decode($_REQUEST['manufacturers']);
            sort($manufacturers_ids);
            $lenses->andWhere(['manufacturer_id' => $manufacturers_ids]);
        }

        if (!empty($_REQUEST['subtypes'])){
            $subtypes = json_decode($_REQUEST['subtypes']);
            sort($subtypes);
            $lenses->andWhere(['l.subtype' => $subtypes]);
        }
        
        // по сроку ношения
        if (!empty($_REQUEST['lens_wearing_time_id'])){
            $lenses->andWhere(['l.wearing_time_id' => $_REQUEST['lens_wearing_time_id']]);
        }
        
        // по режиму ношения
        if (!empty($_REQUEST['lens_wearing_mode_id'])){
            $lenses->andWhere(['l.wearing_mode_id' => $_REQUEST['lens_wearing_mode_id']]);
        }      

        // сортировка
        if (empty($_REQUEST['sortby']) || strcmp($_REQUEST['sortby'],'popularity')==0)
            $lenses->orderBy(['views_amount'=> SORT_DESC]);
        else {
            switch($_REQUEST['sortby']){
                case 'brend_az':
                    $lenses->orderBy(['b.name'=> SORT_ASC]);
                    break;
                case 'brend_za':
                    $lenses->orderBy(['b.name'=> SORT_DESC]);
                    break;
                case 'price_inc':
                    $lenses->orderBy(['price'=> SORT_ASC]);
                    break;
                case 'price_dec':
                    $lenses->orderBy(['price'=> SORT_DESC]);
                    break;
            }
        }
        $countQuery = clone $lenses;
        $totalCount = $countQuery->count();
        $models = $lenses->offset(($_REQUEST['page']-1)*30)
            ->limit(30)
            ->all();
        echo json_encode(array(
            'html'=>$this->renderAjax('list',array(
                    'lenses' => $models  
                )
            ),
            'count' => $totalCount
        ));
    }
    
    // Карточка товара
    public function actionProduct(){
        if (empty($_GET['id']))
            throw new HttpException(404, 'Not Found');
        else {
            $product = Goods::find()
                ->where(['published' => 1,'good_type' => 3, 'goods.id' => (int)$_GET['id']])
                ->one();
            if (empty($product))
                throw new HttpException(404, 'Product Does Not Exist');
            $product->views_amount++; // Увеличение счетчика просмотров
            $product->save();
            $history_ids = Goods::getHistory(); // Получение списка просмотренных товаров
            $history = Goods::find()
                    ->where(['id' => $history_ids, 'published' => 1])
                    ->all();

            Goods::toHistory($_GET['id']); // Добавление в историю просмотренных товаров


            
            
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => "КОНТАКТНЫЕ ЛИНЗЫ",
                'url' => '/lenses',
                'template' => '<span>{link}</span> - '
            ];
            
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => mb_strtoupper($product->name, "utf-8"),
                'template' => '<span>{link}</span>'
            ];
            // Фотографии товара
            $photos = Photos::find()
                    ->where(['good_id' => (int)$_GET['id']])
                    ->offset(0)
                    ->limit(4)
                    ->all();
            $this->view->registerCssFile('/resources/css/similar.css');
            $this->view->registerCssFile('/resources/css/history.css');
            $this->view->registerCssFile('/resources/css/lenses_product.css');
            $this->view->registerJsFile('/resources/js/lenses_product.js',['depends'=>'yii\web\JqueryAsset']);

            $similar = Goods::find()
                ->where(['published' => 1,'good_type' => 3, 'goods.sku' => $product->sku])
                ->andWhere(['<>', 'id', $product->id])
                ->all();
            
            if (strcmp($product->lenses->subtype, 'spherical') == 0)
                $type = 'сферические';
            else
                $type = 'астигматические';
            return $this->render('product', [
                'product' => $product,
                'photos' => $photos,
                'similar' => $similar,
                'history' => $history,
                'type' => $type]
                /*'sorter_by_array' => $sorters_array,
                'brands' => $brands,
                'forms' => $forms,
                'frame_types' => $frame_types,
                'frame_materials' => $frame_materials,
                'lens_colors' => $lens_colors,
                'frame_colors' => $frame_colors
            ]*/);
        }
    }
}
