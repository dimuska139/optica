<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use \app\models\Goods;
use \app\models\Brands;
use \app\models\Photos;
use \app\models\Related;
use \app\models\Manufacturers;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;


class RelatedController extends Controller
{
    // Заполнение тестовыми данными
    public function actionFill(){
        $brands = array_keys(ArrayHelper::map(Brands::find()->all(), 'id', 'name'));
        $manufacturers = array_keys(ArrayHelper::map(Manufacturers::find()->all(), 'id', 'name'));
        $materials = array_keys(ArrayHelper::map(Materials::find()->all(), 'id', 'name'));
        for ($i=0;$i<500;$i++){
            $brand_id = $brands[array_rand($brands)];
            $good = new Goods();
            $subtypes = ['solution','drops','set','case','napkin','spray'];
            $subtype = $subtypes[array_rand($subtypes)];
            $name = "Раствор ";
            switch($subtype){
                case 'solution':
                    $name = "Раствор ";
                    break;
                case 'drops':
                    $name = "Увлажняющие капли ";
                    break;
                case 'set':
                    $name = "Набор для линз ";
                    break;
                case 'case':
                    $name = "Футляр ";
                    break;
                case 'napkin':
                    $name = "Салфетки ";
                    break;
                case 'spray':
                    $name = "Спрей ";
                    break;
            }
            $good->name = $name.$i;
            $good->description = "Модуль Сопутствующие товары позволяет Вашим клиентам обратить внимание, а то и прикупить заодно с выбранным товаром, сопутствующий ему.
Например, в вашем магазине имеются в продаже удочки, катушки, леска и прочие снасти. К категории \"Удочки\" довольно уместно привязать категории \"Леска\", \"Катушки\", - в таком случае, пользователь находясь в карточке товара выбранного заветного удилища будет видеть блок сопутствующий товаров(аксессуаров), что непременно повлечет к покупке \"крючка и лески\" к удочке. Или же у Вас в продаже Фотокамеры, - обязательно нужно предложить к фотокамере приобрести объектив, сумку-чехол, карту памяти и т.д.";
            $logo = "solution.jpg";
            switch($subtype){
                case 'solution':
                    $logo = "solution.jpg";
                    break;
                case 'drops':
                    $logo = "drops.jpg";
                    break;
                case 'set':
                    $logo = "set.jpg";
                    break;
                case 'case':
                    $logo = "case.jpg";
                    break;
                case 'napkin':
                    $logo = "napkin.jpg";
                    break;
                case 'spray':
                    $logo = "spray.jpg";
                    break;
            }
            $good->logo = $logo;
            $good->price = rand(100,3000);
            $good->good_type = 4;
            $good->brand_id = $brand_id;
            $good->views_amount = rand(0,1000);
            $good->published = 1;
            $good->owner_id = 1;
            $good->ip = ip2long($_SERVER['REMOTE_ADDR']);
            $good->sku = (string)rand(10000,99999);
            $good->new = rand(0,1);
            $manufacturer_id = $manufacturers[array_rand($manufacturers)];
            $good->manufacturer_id = $manufacturer_id;
            $good->amount = rand(0,100);
            $good->rand_sort = rand(0,100000);
            if (!$good->save()){
                var_dump($good->errors);
                die;
            }
            
            $photo = new Photos();
            $photo->photo_name = $logo;
            $photo->good_id = $good->id;
            if (!$photo->save()){
                var_dump($photo->errors);
                die;
            }
            
            $rel = new Related();
            $rel->subtype = $subtype;
            if (in_array($subtype, array('solution', 'spray', 'napkin', 'drops'))){
                $rel->capacity = rand(10,100);
            } else {
                $rel->country = "Россия";
            }
            
            $rel->goodID = $good->id;
            if (!$rel->save()){
                var_dump($rel->errors);
                die;
            }
        }
        die;
    }
    
    public function actionIndex()
    {   
        $sorters_array = array();
        $sorters_array['popularity'] = 'Популярность';
        $sorters_array['brend_az'] = 'Бренд (A-Z)';
        $sorters_array['brend_za'] = 'Бренд (Z-A)';
        $sorters_array['price_inc'] = 'Цена (по возрастанию)';
        $sorters_array['price_dec'] = 'Цена (по убыванию)';
        Yii::$app->view->params['breadcrumbs'][] = [
            'label' => 'СОПУТСТВУЮЩИЕ ТОВАРЫ',
            'template' => '<span>{link}</span>'
        ];
        $this->view->registerCssFile('resources/css/related.css');
        $this->view->registerJsFile('resources/js/jquery.json.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/optica_paginator.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/related.js',['depends'=>'yii\web\JqueryAsset']);
        $manufacturers = Manufacturers::find()->orderBy(['name'=> SORT_ASC])->all();
        return $this->render('index', [
            'manufacturers' => $manufacturers,
            'sorter_by_array' => $sorters_array
        ]);
    }
    // Получение списка
    public function actionGetlist(){
        $related = Goods::find()
           ->leftJoin('brands b', 'brand_id=b.id')
            ->leftJoin('related rltd', 'goods.id=rltd.goodID');
        $where = array('published' => 1);
        $where['good_type'] = 4;
        $related->where($where);

        
        if (!empty($_REQUEST['new']))
            $related->andWhere('`new`=1');
        
        // цена от
        if (!empty($_REQUEST['price_from']))
            $related->andWhere('`price`>:price_from', [':price_from'=>$_REQUEST['price_from']]);

        // цена до
        if (!empty($_REQUEST['price_to']))
            $related->andWhere('`price`<:price_to', [':price_to'=>$_REQUEST['price_to']]);

        // производители
        if (!empty($_REQUEST['manufacturers'])){
            $manufacturers_ids = json_decode($_REQUEST['manufacturers']);
            sort($manufacturers_ids);
            $related->andWhere(['manufacturer_id' => $manufacturers_ids]);
        }

        if (!empty($_REQUEST['subtypes'])){
            $subtypes = json_decode($_REQUEST['subtypes']);
            sort($subtypes);
            $related->andWhere(['rltd.subtype' => $subtypes]);
        }

        // сортировка
        if (empty($_REQUEST['sortby']) || strcmp($_REQUEST['sortby'],'popularity')==0)
            $related->orderBy(['views_amount'=> SORT_DESC]);
        else {
            switch($_REQUEST['sortby']){
                case 'brend_az':
                    $related->orderBy(['b.name'=> SORT_ASC]);
                    break;
                case 'brend_za':
                    $related->orderBy(['b.name'=> SORT_DESC]);
                    break;
                case 'price_inc':
                    $related->orderBy(['price'=> SORT_ASC]);
                    break;
                case 'price_dec':
                    $related->orderBy(['price'=> SORT_DESC]);
                    break;
            }
        }
        $countQuery = clone $related;
        $totalCount = $countQuery->count();
        $models = $related->offset(($_REQUEST['page']-1)*30)
            ->limit(30)
            ->all();
        echo json_encode(array(
            'html'=>$this->renderAjax('list',array(
                    'related' => $models  
                )
            ),
            'count' => $totalCount
        ));
    }
    
    // Карточка товара
    public function actionProduct(){
        if (empty($_GET['id']))
            throw new HttpException(404, 'Not Found');
        else {
            $product = Goods::find()
                ->where(['published' => 1,'good_type' => 4, 'goods.id' => (int)$_GET['id']])
                ->one();
            if (empty($product))
                throw new HttpException(404, 'Product Does Not Exist');
            $product->views_amount++; // Увеличение количества просмотров
            $product->save();
            $history_ids = Goods::getHistory(); // Получение списка просмотренных товаров
            $history = Goods::find()
                    ->where(['id' => $history_ids, 'published' => 1])
                    ->all();

            Goods::toHistory($_GET['id']); // Добавление в историю просмотров


            
            // Формирование "Хлебных крошек"
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => "СОПУТСТВУЮЩИЕ ТОВАРЫ",
                'url' => '/related',
                'template' => '<span>{link}</span> - '
            ];
            
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => mb_strtoupper($product->name, "utf-8"),
                'template' => '<span>{link}</span>'
            ];
            // Получение фотографий товара
            $photos = Photos::find()
                    ->where(['good_id' => (int)$_GET['id']])
                    ->offset(0)
                    ->limit(4)
                    ->all();
            $this->view->registerCssFile('/resources/css/similar.css');
            $this->view->registerCssFile('/resources/css/history.css');
            $this->view->registerCssFile('/resources/css/related_product.css');
            $this->view->registerJsFile('/resources/js/related_product.js',['depends'=>'yii\web\JqueryAsset']);

            // Похожие товары
            $similar = Goods::find()
                ->where(['published' => 1,'good_type' => 4, 'goods.sku' => $product->sku])
                ->andWhere(['<>', 'id', $product->id])
                ->all();
            
            return $this->render('product', [
                'product' => $product,
                'photos' => $photos,
                'similar' => $similar,
                'history' => $history]
                /*'sorter_by_array' => $sorters_array,
                'brands' => $brands,
                'forms' => $forms,
                'frame_types' => $frame_types,
                'frame_materials' => $frame_materials,
                'lens_colors' => $lens_colors,
                'frame_colors' => $frame_colors
            ]*/);
        }
    }
}
