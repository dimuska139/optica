<?php

namespace app\controllers;
use Yii;
use app\models\News;
use yii\web\HttpException;

class NewsController extends \yii\web\Controller
{
    // Заполнение тестовыми данными
    public function actionFill(){
        die;
        for ($i=0;$i<500;$i++){
            $news = new News();
            $news->title = "Формы солнечных очков. Виды солнцезащитных очков.";
            $news->logo = "http://top-sovet.ru/sites/default/files/styles/300x200/public/field/field_img/formy_solnechnyh_ochkov._vidy_solncezashchitnyh_ochkov.jpg?itok=Qd_pn9v6";
            $news->text = '<p>Солнцезащитные очки или солнечные очки, используются, чтобы защитить глаза от ультрафиолетового излучения и синего света, который может вызвать ряд серьезных проблем для глаз, а также в качестве модного аксессуара, особенно на пляже.</p>
<p>Дизайн солнечных очков разнообразен, однако некоторые модели стали «классикой жанра» по названию, данному производителем.</p>
<h2>Броулайнеры (Browline)</h2>
<p class="clearfix"><a class="colorbox init-colorbox-processed cboxElement" href="http://top-sovet.ru/sites/default/files/pictures/user4/browline.png" rel="sun" title="Формы солнечных очков. Виды солнцезащитных очков."><img alt="Формы солнечных очков. Виды солнцезащитных очков." class="ithird ileft" src="http://top-sovet.ru/sites/default/files/pictures/user4/browline.png" title="Формы солнечных очков. Виды солнцезащитных очков."></a>Броулайнеры – это очки с утолщенной верхней частью оправы, отсюда и название. Появились в 1950-е в Америке. Броулайнеры будут нелепо смотреться со спортивным костюмом или джинсами, но зато они отлично подходят под костюм для деловых переговоров.</p>
<h2>Авиаторы (Aviator)</h2>
<p class="clearfix"><a class="colorbox init-colorbox-processed cboxElement" href="http://top-sovet.ru/sites/default/files/pictures/user4/aviator.png" rel="sun" title="Формы солнечных очков. Виды солнцезащитных очков."><img alt="Формы солнечных очков. Виды солнцезащитных очков." class="ithird ileft" src="http://top-sovet.ru/sites/default/files/pictures/user4/aviator.png" title="Формы солнечных очков. Виды солнцезащитных очков."></a>Авиаторы – это модель с каплевидными линзами и тонкой металлической оправой. Изобретены в США в 1936 году, специально для летчиков. Кроме летчиков в 1960-1990 г. модель пользовалась популярностью среди молодежи, а также военных. Пользуются большой популярностью среди полицейских.</p>
<h2>Вайфареры (Wayfarer)</h2>
<p class="clearfix"><a class="colorbox init-colorbox-processed cboxElement" href="http://top-sovet.ru/sites/default/files/pictures/user4/wayfarer.png" rel="sun" title="Формы солнечных очков. Виды солнцезащитных очков."><img alt="Формы солнечных очков. Виды солнцезащитных очков." class="ithird ileft" src="http://top-sovet.ru/sites/default/files/pictures/user4/wayfarer.png" title="Формы солнечных очков. Виды солнцезащитных очков."></a>Вайфареры – это очки в роговой или пластмассовой оправе. Изготовлены в США в 1952 г. Они имеют трапециевидную форму, расширяющуюся кверху.</p>
<h2>Кошачий глаз (Cats eye)</h2>
<p class="clearfix"><a class="colorbox init-colorbox-processed cboxElement" href="http://top-sovet.ru/sites/default/files/pictures/user4/cats_eye.png" rel="sun" title="Формы солнечных очков. Виды солнцезащитных очков."><img alt="Формы солнечных очков. Виды солнцезащитных очков." class="ithird ileft" src="http://top-sovet.ru/sites/default/files/pictures/user4/cats_eye.png" title="Формы солнечных очков. Виды солнцезащитных очков."></a>Кошачий глаз – это очки в толстой роговой оправе с заостренными верхними углами. В основном используются женщинами. Существует разновидность – “стрекоза”, с большими круглыми или квадратными линзами.</p>
<h2>Тишейды (Teashades). Круглые очки.</h2>
<p class="clearfix"><a class="colorbox init-colorbox-processed cboxElement" href="http://top-sovet.ru/sites/default/files/pictures/user4/kruglye.png" rel="sun" title="Формы солнечных очков. Виды солнцезащитных очков."><img alt="Формы солнечных очков. Виды солнцезащитных очков." class="ithird ileft" src="http://top-sovet.ru/sites/default/files/pictures/user4/kruglye.png" title="Формы солнечных очков. Виды солнцезащитных очков."></a>Это маленькие круглые линзы в тонкой проволочной оправе. Носились в 60-е просто для красоты, или как знак принадлежности к субкультуре хиппи. Кот Базиллио, гарцевал именно в таких очках.</p>
<h2>Солнечные очки для активного образа жизни.</h2>
<p><a class="colorbox init-colorbox-processed cboxElement" href="http://top-sovet.ru/sites/default/files/pictures/user4/daddy-o.png" rel="sun" title="Формы солнечных очков. Виды солнцезащитных очков."><img alt="Формы солнечных очков. Виды солнцезащитных очков." class="ithird ileft" src="http://top-sovet.ru/sites/default/files/pictures/user4/daddy-o.png" title="Формы солнечных очков. Виды солнцезащитных очков."></a>Обычно это облегающие очки, которые могут состоять из одной линзы, изогнутой полукругом и тонкой пластиковой оправы, которая находится только на месте перемычки или с двумя линзами в пластиковой оправе. Обычно используются для занятия спортом.</p>';
            $news->published = 1;
            $news->owner_id = 1;
            $news->save();
        }
    }
    // Получение списка новостей
    public function actionGetlist(){
        $news = News::find();
        $where = array('published' => 1);
        $news->where($where);
        $news->orderBy(['create_date'=> SORT_DESC]);

        $countQuery = clone $news;
        $totalCount = $countQuery->count();
        $models = $news->offset(($_REQUEST['page']-1)*10)
            ->limit(10)
            ->all();
        $html = $this->renderAjax('list',array(
                    'news' => $models  
                ));
        $convertedHtml = mb_convert_encoding($html, 'utf-8', mb_detect_encoding($html));
        echo json_encode(array(
            'html'=>$convertedHtml,
            'count' => $totalCount
        ));
    }
    
    
    public function actionIndex()
    {   
        Yii::$app->view->params['breadcrumbs'][] = [
            'label' => 'НОВОСТИ',
            'template' => '<span>{link}</span>'
        ];
        $this->view->registerCssFile('/resources/css/news.css');
        $this->view->registerJsFile('/resources/js/jquery.json.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/optica_paginator.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('/resources/js/news.js',['depends'=>'yii\web\JqueryAsset']);
        return $this->render('index');
    }
    // Просмотр определенной новости
    public function actionView(){
        if (empty($_GET['id']))
            throw new HttpException(404, 'Not Found');
        else {
            $news = News::find()
                ->where(['published' => 1, 'news.id' => (int)$_GET['id']])
                ->one();
            if (empty($news))
                throw new HttpException(404, 'News Does Not Exist');
            // Формирование хлебных крошек
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => "НОВОСТИ",
                'url' => '/news',
                'template' => '<span>{link}</span> - '
            ];
            $notagstitle = strip_tags($news->title);
            $label = strlen($notagstitle)>20?mb_strimwidth($notagstitle, 0, 20).'...':$notagstitle;
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => mb_strtoupper($label, "utf-8"),
                'template' => '<span>{link}</span>'
            ];
            
            $this->view->registerCssFile('/resources/css/news_view.css');
        //    $this->view->registerJsFile('/resources/js/sunglasses_product.js',['depends'=>'yii\web\JqueryAsset']);

            
            return $this->render('view', [
                'news' => $news
            ]);
        }
    }
}
