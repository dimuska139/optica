<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\Goods;

class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'foreColor'=>0x66534F,
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        $this->view->registerCssFile('resources/css/index.css');
    //    $this->view->registerCssFile('resources/css/slider.css');
        $this->view->registerJsFile('resources/js/index.js', ['depends'=>'yii\web\JqueryAsset']);
        $new = Goods::find()
                ->where([
                    'new' => 1
                ])
                ->orderBy(['rand_sort' => SORT_DESC])
                ->limit(12)
                ->all();
        return $this->render('index',[
            'new' => $new
        ]);
    }
}
