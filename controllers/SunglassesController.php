<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use \app\models\Goods;
use \app\models\Colors;
use \app\models\Sunglasses;
use \app\models\Brands;
use \app\models\Forms;
use \app\models\Photos;
use \app\models\Materials;
use \app\models\FramesTypes;
use \app\models\Manufacturers;
use yii\helpers\ArrayHelper;
use yii\web\HttpException;


class SunglassesController extends Controller
{
// Заполнение тестовыми данными
    public function actionFill(){
        die;
        $brands = array_keys(ArrayHelper::map(Brands::find()->all(), 'id', 'name'));
        $forms = array_keys(ArrayHelper::map(Forms::find()->all(), 'id', 'name'));
        $frame_types = array_keys(ArrayHelper::map(FramesTypes::find()->all(), 'id', 'name'));
        $materials = array_keys(ArrayHelper::map(Materials::find()->all(), 'id', 'name'));
        $colors = array_keys(ArrayHelper::map(Colors::find()->all(), 'id', 'name'));
        $manufacturers = array_keys(ArrayHelper::map(Manufacturers::find()->all(), 'id', 'name'));
        for ($i=0;$i<500;$i++){
            $brand_id = $brands[array_rand($brands)];
            $good = new Goods();
            $good->name = "Очки ".$i;
            $good->description = "Очки Ray Ban В середине прошлого столетия, когда мода пестрила сочными фруктовыми цветами, и жизнь проходила в ритме джаза и рок-н-ролла, компания Ray-Ban покорившая мир солнечными очками ray ban aviator, завоевавшими всеобщую любовь, создала новую модель оптики, оправа которой была сделана из высококачественного пластика, - очки ray ban wayfarer. Форма стекол – дерзкая и привлекающая внимание - отражала все модные тенденции той эпохи. Сегодня миллионы людей во всем мире отдают предпочтение бренду ray ban. Очки солнцезащитные ray ban сделаны настолько универсально (и из экологически чистых материалов), что подойдут практически каждому. Купить очки ray ban несложно: достаточно зайти в наш интернет магазин, где всегда широкий ассортимент и постоянно действующие скидки. Гармоничное сочетание формы с изящными и лаконичными деталями, непревзойденное качество и стиль магнетически притягивают, и ты уже не можешь не купить ray ban. Мода меняется, но Ray Ban – бренд на все времена, выбор активных и уверенных в себе людей, любящих жизнь.";
            $good->logo = "logo1.jpg";
            $good->price = rand(1000,12000);
            $good->good_type = 1;
            $good->brand_id = $brand_id;
            $good->views_amount = rand(0,1000);
            $good->published = 1;
            $good->owner_id = 1;
            $good->ip = ip2long($_SERVER['REMOTE_ADDR']);
            $good->sku = (string)rand(10000,99999);
            $good->new = rand(0,1);
            $good->amount = rand(0,100);
            $manufacturer_id = $manufacturers[array_rand($manufacturers)];
            $good->manufacturer_id = $manufacturer_id;
            $good->rand_sort = rand(0,100000);
            if (!$good->save()){
                var_dump($good->errors);
                die;
            }
                
            
            
            $sung = new Sunglasses();
            $subtypes = ['male','female','child'];
            $subtype = $subtypes[array_rand($subtypes)];
            $sung->subtype = $subtype; // male - мужские, female - женские, child - детские
            $sung->form_id = $forms[array_rand($forms)];
            $sung->frame_type_id = $frame_types[array_rand($frame_types)];
            $sung->frame_material_id = $materials[array_rand($materials)];
            $sung->lens_color_id = $colors[array_rand($colors)];
            $sung->frame_color_id = $colors[array_rand($colors)];
            
            $sung->goodID = $good->id;
            $sung->save();
        }
        die;
        
    }

    public function actionIndex()
    {   
        $sorters_array = array();
        $sorters_array['popularity'] = 'Популярность';
        $sorters_array['brend_az'] = 'Бренд (A-Z)';
        $sorters_array['brend_za'] = 'Бренд (Z-A)';
        $sorters_array['price_inc'] = 'Цена (по возрастанию)';
        $sorters_array['price_dec'] = 'Цена (по убыванию)';
        

        Yii::$app->view->params['breadcrumbs'][] = [
            'label' => 'СОЛНЦЕЗАЩИТНЫЕ ОЧКИ',
            'template' => '<span>{link}</span>'
        ];
        $this->view->registerCssFile('resources/css/sunglasses.css');
        $this->view->registerJsFile('resources/js/jquery.json.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/optica_paginator.js',['depends'=>'yii\web\JqueryAsset']);
        $this->view->registerJsFile('resources/js/sunglasses.js',['depends'=>'yii\web\JqueryAsset']);
        
        
        $brands = Brands::find()->orderBy(['name'=> SORT_ASC])->all();
        $forms = Forms::find()->orderBy(['name'=> SORT_ASC])->all();
        $frame_types = FramesTypes::find()->orderBy(['name'=> SORT_ASC])->all();
        $frame_materials = Materials::find()
                            ->where(['id' => ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('frame_material_id')
                                                ->distinct()
                                                ->orderBy(['frame_material_id'=> SORT_ASC])
                                                ->all(), 'id', 'frame_material_id'),'frame_material_id')])
                            ->orderBy(['name'=> SORT_ASC])
                            ->all();
        
        // получение всех цветов оправ, с которыми существуют очки 
        $frame_colors = Colors::find()
                            ->where(['id' => ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('frame_color_id')
                                                ->distinct()
                                                ->orderBy(['frame_color_id'=> SORT_ASC])
                                                ->all(), 'id', 'frame_color_id'),'frame_color_id')])
                            ->orderBy(['name'=> SORT_ASC])
                            ->all();
        // получение всех цветов линз, с которыми существуют очки 
        $lens_colors = Colors::find()
                            ->where(['id' => ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('lens_color_id')
                                                ->distinct()
                                                ->orderBy(['lens_color_id'=> SORT_ASC])
                                                ->all(), 'id', 'lens_color_id'),'lens_color_id')])
                            ->orderBy(['name'=> SORT_ASC])
                            ->all();
        
        
        
        return $this->render('index', [
            'sorter_by_array' => $sorters_array,
            'brands' => $brands,
            'forms' => $forms,
            'frame_types' => $frame_types,
            'frame_materials' => $frame_materials,
            'lens_colors' => $lens_colors,
            'frame_colors' => $frame_colors
        ]);
    }

    // Получение списка
    public function actionGetlist(){
        $sunglasses = Goods::find()
                ->leftJoin('brands b', 'brand_id=b.id')
                ->leftJoin('sunglasses sng', 'goods.id=sng.goodID');
        $where = array('published' => 1);
        $where['good_type'] = 1;
        $sunglasses->where($where);
        
        
        if (!empty($_REQUEST['new']))
            $sunglasses->andWhere('`new`=1');
        
        // цена от
        if (!empty($_REQUEST['price_from']))
            $sunglasses->andWhere('`price`>:price_from', [':price_from'=>$_REQUEST['price_from']]);
        
        // цена до
        if (!empty($_REQUEST['price_to']))
            $sunglasses->andWhere('`price`<:price_to', [':price_to'=>$_REQUEST['price_to']]);
        
        // бренды
        if (!empty($_REQUEST['brands'])){
            $brands_ids = json_decode($_REQUEST['brands']);
            sort($brands_ids);
            $sunglasses->andWhere(['brand_id' => $brands_ids]);
        }
        
        // формы
        if (!empty($_REQUEST['forms'])){
            $forms_ids = json_decode($_REQUEST['forms']);
            sort($forms_ids);
            $sunglasses->andWhere(['sng.form_id' => $forms_ids]);
        }
        
        // типы оправ
        if (!empty($_REQUEST['frames'])){
            $frames_ids = json_decode($_REQUEST['frames']);
            sort($frames_ids);
            $sunglasses->andWhere(['sng.frame_type_id' => $frames_ids]);
        }
        
        // материалы оправ
        if (!empty($_REQUEST['materials'])){
            $materials_ids = json_decode($_REQUEST['materials']);
            sort($materials_ids);
            $sunglasses->andWhere(['sng.frame_material_id' => $materials_ids]);
        }
        
        // по полу
        if (!empty($_REQUEST['subtypes'])){
            $subtype_ids = json_decode($_REQUEST['subtypes']);
            sort($subtype_ids);
            $sunglasses->andWhere(['sng.subtype' => $subtype_ids]);
        }
        
        
        // по цвету линз
        if (!empty($_REQUEST['lens_color_id'])){
            $sunglasses->andWhere(['sng.lens_color_id' => (int)$_REQUEST['lens_color_id']]);
        /*    $ids_arr = ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('goodID')
                                                ->distinct()
                                                ->andWhere('`lens_color_id`=:lens_color_id', [':lens_color_id'=>$_REQUEST['lens_color_id']])
                                                ->all(), 'id', 'goodID'),'goodID');
            $sunglasses->andWhere(['goods.id' => $ids_arr]);*/
        }
        
        // по цвету оправы
        if (!empty($_REQUEST['frame_color_id'])){
            $sunglasses->andWhere(['sng.frame_color_id' => (int)$_REQUEST['frame_color_id']]);
       /*     $ids_arr = ArrayHelper::getColumn(ArrayHelper::toArray(Sunglasses::find()
                                                ->select('goodID')
                                                ->distinct()
                                                ->andWhere('`frame_color_id`=:frame_color_id', [':frame_color_id'=>$_REQUEST['frame_color_id']])
                                                ->all(), 'id', 'goodID'),'goodID');
            $sunglasses->andWhere(['goods.id' => $ids_arr]);*/
        }
        
        
        // сортировка
        if (empty($_REQUEST['sortby']) || strcmp($_REQUEST['sortby'],'popularity')==0)
            $sunglasses->orderBy(['views_amount'=> SORT_DESC]);
        else {
            switch($_REQUEST['sortby']){
                case 'brend_az':
                    $sunglasses->orderBy(['b.name'=> SORT_ASC]);
                    break;
                case 'brend_za':
                    $sunglasses->orderBy(['b.name'=> SORT_DESC]);
                    break;
                case 'price_inc':
                    $sunglasses->orderBy(['price'=> SORT_ASC]);
                    break;
                case 'price_dec':
                    $sunglasses->orderBy(['price'=> SORT_DESC]);
                    break;
            }
        }
        $countQuery = clone $sunglasses;
        $totalCount = $countQuery->count();
        $models = $sunglasses->offset(($_REQUEST['page']-1)*30)
            ->limit(30)
            ->all();
        echo json_encode(array(
            'html'=>$this->renderAjax('list',array(
                    'sunglasses' => $models  
                )
            ),
            'count' => $totalCount
        ));
    }
    // Карточка товара
    public function actionProduct(){
        if (empty($_GET['id']))
            throw new HttpException(404, 'Not Found');
        else {
            $product = Goods::find()
                ->where(['published' => 1,'good_type' => 1, 'goods.id' => (int)$_GET['id']])
                ->one();
            if (empty($product))
                throw new HttpException(404, 'Product Does Not Exist');
            $product->views_amount++; // Увеличиваем количество просмотров
            $product->save();
            $history_ids = Goods::getHistory(); // Получение списка просмотренных товаров
            $history = Goods::find()
                    ->where(['id' => $history_ids, 'published' => 1])
                    ->all();

            Goods::toHistory($_GET['id']); // Добавление в список просмотренных товаров
            
            // "Хлебные крошки"
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => "СОЛНЦЕЗАЩИТНЫЕ ОЧКИ",
                'url' => '/sunglasses',
                'template' => '<span>{link}</span> - '
            ];
            
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => mb_strtoupper($product->name, "utf-8"),
                'template' => '<span>{link}</span>'
            ];
            
            $photos = Photos::find() // Получение фотографий товара
                    ->where(['good_id' => (int)$_GET['id']])
                    ->offset(0)
                    ->limit(4)
                    ->all();
            $this->view->registerCssFile('/resources/css/similar.css');
            $this->view->registerCssFile('/resources/css/history.css');
            $this->view->registerCssFile('/resources/css/sunglasses_product.css');
            $this->view->registerJsFile('/resources/js/sunglasses_product.js',['depends'=>'yii\web\JqueryAsset']);
            $subtype = "мужские";
            switch($product->sunglasses->subtype){
                case 'female':
                    $subtype = "женские";
                    break;
                case 'child':
                    $subtype = "детские";
                    break;
            }
            
            $similar = Goods::find() // Похожие товары
                ->where(['published' => 1,'good_type' => 1, 'goods.sku' => $product->sku])
                ->andWhere(['<>', 'id', $product->id])
                ->all();
            
            return $this->render('product', [
                'product' => $product,
                'photos' => $photos,
                'subtype' => $subtype,
                'similar' => $similar,
                'history' => $history]
                /*'sorter_by_array' => $sorters_array,
                'brands' => $brands,
                'forms' => $forms,
                'frame_types' => $frame_types,
                'frame_materials' => $frame_materials,
                'lens_colors' => $lens_colors,
                'frame_colors' => $frame_colors
            ]*/);
        }
    }
}
