<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use app\models\Guestbook;
use app\models\GuestbookForm;

// Контроллер для страницы с отзывами
class GuestbookController extends Controller
{   
    public function actionIndex()
    {
        $comment_form = new GuestbookForm();
        $comment_sent = false;
        $comment = new Guestbook();
        // Сохранение отзыва в базу
        if ($comment_form->load(Yii::$app->request->post()) && $comment_form->validate()) {
            $comment = new Guestbook();
            $comment->subject = $comment_form->subject;
            $comment->name = $comment_form->name;
            $comment->email = $comment_form->email;
            $comment->comment = $comment_form->comment;
            $comment->ip = ip2long($_SERVER['REMOTE_ADDR']);
            $comment->published = 1; // Когда будет админка, должно быть 0, т.к. нужно, чтобы модерировать отзывы перед публикацией
            
            $comment->save();
            $comment_sent = true;
        }
        $comments_all = Guestbook::find()
            ->where([
                'published' => 1])
            ->orderBy(['create_date'=> SORT_DESC])->all();
        $this->view->registerCssFile('resources/css/guestbook.css');
        return $this->render('index',[
            'comments' => $comments_all,
            'comment_form' => $comment_form,
            'comment_sent' => $comment_sent
        ]);
    }

}
