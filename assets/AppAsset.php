<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;
use Yii;
/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
    //    'css/site.css',
    //    'css/main.css',
        'resources/css/slider.css',
        'resources/css/main.css',
        'resources/css/consultant.css',
        'resources/css/optica_buttons_group.css',
        'resources/css/optica_select.css',
        'resources/css/optica_select_panel.css',
        'resources/css/optica_select_slider.css',
        'resources/css/optica_paginator.css'
    ];
    public $js = [
        'resources/js/slider.js',
      //  'resources/js/jquery-2.1.3.min.js',
        'resources/js/jquery.cookie.js',
        'resources/js/jquery-ui.min.js',
        'resources/js/jquery.rotate2.2.min.js',
        'resources/js/consultant.js',
        'resources/js/main.js',
        'resources/js/optica_select.js',
        'resources/js/optica_select_panel.js',
        'resources/js/optica_select_slider.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
    
    public function init()
    {
        parent::init();
        // resetting BootstrapAsset to not load own css files
        Yii::$app->assetManager->bundles['yii\\bootstrap\\BootstrapAsset'] = [
            'css' => []
        ];
    }
}
