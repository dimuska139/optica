<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lenses_wearing_time".
 *
 * @property integer $id
 * @property string $time
 * @property string $create_date
 */
class LensesWearingTime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lenses_wearing_time';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['time'], 'required'],
            [['create_date'], 'safe'],
            [['time'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'time' => 'Time',
            'create_date' => 'Create Date',
        ];
    }
}
