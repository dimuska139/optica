<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property integer $id
 * @property string $login
 * @property string $hash
 * @property string $realname
 * @property string $lastonline_time
 * @property string $photo_url
 * @property string $email
 * @property string $create_date
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['login', 'hash', 'lastonline_time', 'email'], 'required'],
            [['photo_name', 'email'], 'string'],
            [['create_date'], 'safe'],
            [['login', 'realname'], 'string', 'max' => 45],
            [['hash'], 'string', 'max' => 32],
            [['login'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'login' => 'Login',
            'hash' => 'Hash',
            'realname' => 'Realname',
            'lastonline_time' => 'Status',
            'photo_name' => 'Photo Name',
            'email' => 'Email',
            'create_date' => 'Create Date',
        ];
    }
}
