<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "frame_types".
 *
 * @property integer $id
 * @property string $name
 * @property string $img_url
 * @property string $create_date
 */
class FramesTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'frames_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['img_url'], 'string'],
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'img_url' => 'Img Url',
            'create_date' => 'Create Date',
        ];
    }
}
