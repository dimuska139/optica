<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class CartForm extends Model
{
    public $name;
    public $surname;
    public $delivery;
    public $payment;
    public $email;
    public $retypeEmail;
    public $phone;
    public $address;
    public $index;
    public $city;
    public $region;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['surname','name','delivery','payment',
                'email','retypeEmail','phone','address',
                'index','city','region'], 'required', 'message' => 'Поле не может быть пустым'],/*,
            ['verifyCode', 'captcha', 'message' => 'Код с картинки введён неверно'],*/
            ['email', 'email', 'message' => 'Укажите, пожалуйста, настоящий e-mail'],
            ['retypeEmail', 'compare', 'compareAttribute' => 'email', 'message' => 'Указанные e-mail должны совпадать']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'ИМЯ',
            'surname' => 'ФАМИЛИЯ',
            'delivery' => 'Доставка',
            'payment' => 'Оплата',
            'email' => 'E-MAIL',
            'retypeEmail' => 'Подтвердите E-MAIL',
            'phone' => 'Номер телефона',
            'address' => 'Адрес',
            'index' => 'Индекс',
            'city' => 'Город',
            'region' => 'Регион',
        ];
    }
}
