<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "related".
 *
 * @property integer $id
 * @property string $subtype
 * @property integer $capacity
 * @property string $country
 * @property string $create_date
 * @property integer $goodID
 */
class Related extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'related';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subtype', 'goodID'], 'required'],
            [['capacity', 'goodID'], 'integer'],
            [['country'], 'string'],
            [['create_date'], 'safe'],
            [['subtype'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subtype' => 'Subtype',
            'capacity' => 'Capacity',
            'country' => 'Country',
            'create_date' => 'Create Date',
            'goodID' => 'Good ID',
        ];
    }
}
