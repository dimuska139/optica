<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "frames".
 *
 * @property integer $id
 * @property string $subtype
 * @property integer $form_id
 * @property integer $frame_type_id
 * @property integer $frame_material_id
 * @property integer $frame_color_id
 * @property string $create_date
 * @property integer $goodID
 */
class Frames extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'frames';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subtype', 'form_id', 'frame_type_id', 'frame_material_id', 'frame_color_id', 'goodID'], 'required'],
            [['form_id', 'frame_type_id', 'frame_material_id', 'frame_color_id', 'goodID'], 'integer'],
            [['create_date'], 'safe'],
            [['subtype'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subtype' => 'Subtype',
            'form_id' => 'Form ID',
            'frame_type_id' => 'Frame Type ID',
            'frame_material_id' => 'Frame Material ID',
            'frame_color_id' => 'Frame Color ID',
            'create_date' => 'Create Date',
            'goodID' => 'Good ID',
        ];
    }
    
    public function getForm(){
        return $this->hasOne(Forms::className(), ['id' => 'form_id']);
    }
    
    public function getFramematerial(){
        return $this->hasOne(Materials::className(), ['id' => 'frame_material_id']);
    }
    
    public function getFramecolor(){
        return $this->hasOne(Colors::className(), ['id' => 'frame_color_id']);
    }
}
