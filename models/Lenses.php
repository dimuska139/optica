<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lenses".
 *
 * @property integer $id
 * @property integer $goodID
 * @property string $subtype
 * @property integer $wearing_time_id
 * @property integer $wearing_mode_id
 * @property integer $blisters_amount
 * @property integer $moisture_content
 * @property integer $material_id
 * @property integer $transmittance_of_oxygen
 */
class Lenses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lenses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['goodID', 'subtype', 'wearing_time_id', 'wearing_mode_id', 'blisters_amount', 'moisture_content', 'material_id', 'transmittance_of_oxygen'], 'required'],
            [['goodID', 'wearing_time_id', 'wearing_mode_id', 'blisters_amount', 'moisture_content', 'material_id', 'transmittance_of_oxygen'], 'integer'],
            [['subtype'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goodID' => 'Good ID',
            'subtype' => 'Subtype',
            'wearing_time_id' => 'Wearing Time ID',
            'wearing_mode_id' => 'Wearing Mode ID',
            'blisters_amount' => 'Blisters Amount',
            'moisture_content' => 'Moisture Content',
            'material_id' => 'Material ID',
            'transmittance_of_oxygen' => 'Transmittance Of Oxygen',
        ];
    }
    
    public function getWearingtime(){
        return $this->hasOne(LensesWearingTime::className(), ['id'=>'wearing_time_id']);
    }
    
    public function getWearingmode(){
        return $this->hasOne(LensesWearingMode::className(), ['id'=>'wearing_mode_id']);
    }
    
    public function getMaterial(){
        return $this->hasOne(Materials::className(), ['id'=>'material_id']);
    }
}
