<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goods_promotions".
 *
 * @property integer $id
 * @property integer $promotionID
 * @property integer $goodID
 * @property integer $active
 * @property string $create_date
 */
class GoodsPromotions extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods_promotions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['promotionID', 'goodID', 'active'], 'required'],
            [['promotionID', 'goodID', 'active'], 'integer'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'promotionID' => 'Promotion ID',
            'goodID' => 'Good ID',
            'active' => 'Active',
            'create_date' => 'Create Date',
        ];
    }
}
