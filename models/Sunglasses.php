<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sunglasses".
 *
 * @property integer $id
 * @property string $subtype
 * @property integer $form_id
 * @property integer $frame_type_id
 * @property integer $frame_material_id
 * @property integer $lens_color_id
 * @property integer $frame_color_id
 * @property integer $lens_material_id
 * @property string $create_date
 * @property integer $goodID
 */
class Sunglasses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sunglasses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subtype', 'form_id', 'frame_type_id', 'frame_material_id', 'lens_color_id', 'lens_material_id', 'frame_color_id', 'goodID'], 'required'],
            [['form_id', 'frame_type_id', 'frame_material_id', 'lens_color_id', 'lens_material_id', 'frame_color_id', 'goodID'], 'integer'],
            [['create_date'], 'safe'],
            [['subtype'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subtype' => 'Subtype',
            'form_id' => 'Form ID',
            'frame_type_id' => 'Frame Type ID',
            'frame_material_id' => 'Frame Material ID',
            'lens_color_id' => 'Lens Color ID',
            'frame_color_id' => 'Frame Color ID',
            'lens_material_id' => 'Lens Material ID',
            'create_date' => 'Create Date',
            'goodID' => 'Good ID',
        ];
    }
    
    public function getFramematerial(){
        return $this->hasOne(Materials::className(), ['id' => 'frame_material_id']);
    }
    
    public function getLensesmaterial(){
        return $this->hasOne(Materials::className(), ['id' => 'lens_material_id']);
    }
    
    public function getFramecolor(){
        return $this->hasOne(Colors::className(), ['id' => 'frame_color_id']);
    }
    
    public function getLensescolor(){
        return $this->hasOne(Colors::className(), ['id' => 'lens_color_id']);
    }
}
