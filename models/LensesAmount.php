<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lenses_amount".
 *
 * @property integer $id
 * @property integer $lensesID
 * @property integer $colorID
 * @property integer $amount
 * @property string $create_date
 */
class LensesAmount extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lenses_amount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lensesID', 'colorID', 'amount'], 'required'],
            [['lensesID', 'colorID', 'amount'], 'integer'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'lensesID' => 'Lenses ID',
            'colorID' => 'Color ID',
            'amount' => 'Amount',
            'create_date' => 'Create Date',
        ];
    }
}
