<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "consultant_correspondence".
 *
 * @property integer $id
 * @property integer $admin_id
 * @property integer $ip
 * @property string $attachment_url
 * @property string $message
 * @property string $user_name
 * @property string $create_date
 */
class ConsultantCorrespondence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'consultant_correspondence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['admin_id', 'ip'], 'integer'],
            [['ip', 'message', 'user_name'], 'required'],
            [['attachment_url'], 'string'],
            [['create_date'], 'safe'],
            [['message'], 'string', 'max' => 1024],
            [['user_name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'admin_id' => 'Admin ID',
            'ip' => 'Ip',
            'attachment_url' => 'Attachment Url',
            'message' => 'Message',
            'user_name' => 'User Name',
            'create_date' => 'Create Date',
        ];
    }
}
