<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $title
 * @property string $logo
 * @property string $text
 * @property integer $published
 * @property string $create_date
 * @property integer $owner_id
 */
class News extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'text', 'owner_id'], 'required'],
            [['logo', 'text'], 'string'],
            [['published', 'owner_id'], 'integer'],
            [['create_date'], 'safe'],
            [['title'], 'string', 'max' => 300]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'logo' => 'Logo',
            'text' => 'Text',
            'published' => 'Published',
            'create_date' => 'Create Date',
            'owner_id' => 'Owner ID',
        ];
    }
}
