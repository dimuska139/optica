<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "frames_sizes".
 *
 * @property integer $id
 * @property string $size
 * @property string $create_date
 */
class FramesSizes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'frames_sizes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['size'], 'required'],
            [['create_date'], 'safe'],
            [['size'], 'string', 'max' => 200]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'size' => 'Size',
            'create_date' => 'Create Date',
        ];
    }
}
