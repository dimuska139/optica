<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "goods".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $logo
 * @property string $price
 * @property integer $good_type
 * @property integer $brand_id
 * @property integer $views_amount
 * @property integer $published
 * @property integer $amount
 * @property integer $owner_id
 * @property string $create_date
 * @property integer $ip
 * @property string $sku
 * @property integer $new
 * @property integer $manufacturer_id
 * @property integer $rand_sort
 */
class Goods extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'goods';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'price', 'good_type', 'brand_id', 'amount', 'owner_id', 'ip', 'sku', 'new', 'manufacturer_id', 'rand_sort'], 'required'],
            [['description', 'logo', 'sku'], 'string'],
            [['price'], 'number'],
            [['good_type', 'brand_id', 'views_amount', 'published', 'amount', 'owner_id', 'ip', 'new', 'manufacturer_id', 'rand_sort'], 'integer'],
            [['create_date'], 'safe'],
            [['name'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'logo' => 'Logo',
            'price' => 'Price',
            'good_type' => 'Good Type',
            'brand_id' => 'Brand ID',
            'views_amount' => 'Views Amount',
            'published' => 'Published',
            'amount' => 'Amount',
            'owner_id' => 'Owner ID',
            'create_date' => 'Create Date',
            'ip' => 'Ip',
            'sku' => 'Sku',
            'new' => 'New',
            'manufacturer_id' => 'Manufacturer ID',
            'rand_sort' => 'Rand Sort',
        ];
    }
    
    // Добавление товара в историю просмотров
    static function toHistory($id){
        $history_arr = array();
        if (!empty($_COOKIE['history'])){
            $history_arr = json_decode($_COOKIE['history'], true);
            if (!in_array($id, $history_arr)){
                array_unshift($history_arr, $id); // Вставка в начало
                if (count($history_arr)>6)
                    array_pop($history_arr); // Удаление последнего
            } else {
                $key = array_search($id, $history_arr);
                unset($history_arr[$key]);
                sort($history_arr);
                array_unshift($history_arr, $id); 
            }
        } else {
            $history_arr[] = $id;
        }
        $history_json = json_encode($history_arr);
        setcookie('history', $history_json, -1, '/');
    }
    
    static function getHistory(){
        $history_arr = array();
        if (!empty($_COOKIE['history']))
            $history_arr = json_decode($_COOKIE['history'], true);
        return $history_arr;
    }
    
    public function getBrand(){
        return $this->hasOne(Brands::className(), ['id' => 'brand_id']);
    }
    
    public function getManufacturer(){
        return $this->hasOne(Manufacturers::className(), ['id' => 'manufacturer_id']);
    }
    
    public function getSunglasses(){
        return $this->hasOne(Sunglasses::className(), ['goodID'=>'id']);
    }
    
    public function getFrames(){
        return $this->hasOne(Frames::className(), ['goodID' => 'id']);
    }
    
    public function getLenses(){
        return $this->hasOne(Lenses::className(), ['goodID' => 'id']);
    }
    
    public function getRelated(){
        return $this->hasOne(Related::className(), ['goodID'=>'id']);
    }
}
