<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "lenses_wearing_mode".
 *
 * @property integer $id
 * @property string $mode
 * @property string $create_date
 */
class LensesWearingMode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'lenses_wearing_mode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['mode'], 'required'],
            [['create_date'], 'safe'],
            [['mode'], 'string', 'max' => 1024]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'mode' => 'Mode',
            'create_date' => 'Create Date',
        ];
    }
}
