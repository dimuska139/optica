<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class GuestbookForm extends Model
{
    public $subject;
    public $name;
    public $email;
    public $comment;
    public $verifyCode;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject','name', 'email', 'comment'], 'required', 'message' => 'Поле не может быть пустым'],
            ['verifyCode', 'captcha', 'message' => 'Код с картинки введён неверно'],
            ['email', 'email', 'message' => 'Укажите, пожалуйста, настоящий e-mail'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'verifyCode' => 'Проверочный код',
            'subject' => 'ТЕМА',
            'name' => 'ИМЯ',
            'email' => 'E-MAIL',
            'comment' => 'ТЕКСТ'
        ];
    }
}
