<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "photos".
 *
 * @property integer $id
 * @property string $photo_name
 * @property integer $good_id
 * @property string $create_date
 */
class Photos extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'photos';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['photo_name', 'good_id'], 'required'],
            [['photo_name'], 'string'],
            [['good_id'], 'integer'],
            [['create_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'photo_name' => 'Photo Name',
            'good_id' => 'Good ID',
            'create_date' => 'Create Date',
        ];
    }
}
