<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "guestbook".
 *
 * @property integer $id
 * @property string $subject
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property string $create_date
 * @property integer $published
 */
class Guestbook extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'guestbook';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['subject', 'name', 'email', 'comment', 'published'], 'required'],
            [['comment'], 'string'],
            [['create_date'], 'safe'],
            [['published'], 'integer'],
            [['subject', 'name'], 'string', 'max' => 1024],
            [['email'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'subject' => 'Topic',
            'name' => 'Name',
            'email' => 'Email',
            'comment' => 'Comment',
            'create_date' => 'Create Date',
            'published' => 'Published',
        ];
    }
}
