<?php
    use yii\helpers\Html;
    use yii\bootstrap\Nav;
    use yii\bootstrap\NavBar;
    use yii\widgets\Breadcrumbs;
    use app\assets\AppAsset;
    /* @var $this \yii\web\View */
    /* @var $content string */

    AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="Content-Language" content="ru"/>
<!--    <meta name="viewport" content="width=device-width, initial-scale=1.0" /> -->
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>

<body>
    <?php $this->beginBody() ?>
    <div id="main-container">
        <div id="header">
            <a href="/"><div id="logo"></div></a>
            <div class="address">
                    Санкт-Петербург, проспект Ленина 13
                    <br>
                    пн-сб с 9.00 до 22.00
                    <br>
                    <span class="phone-text">Тел. 8(911)1234567</span>
                    <br>
                    grali-6@mail.ru
                    <br>
            </div>
            <div class="search-and-basket">
                <a href="/cart">
                <div id="basket">
                    <img class="icon" src="/resources/img/cart-icon.png">
                    <div class="field">
                        <span></span>
                    </div>
                    <div class="counter">
                        <span></span>
                    </div>
                </div>
                </a>
                <div id="search">
                    <form action="/search" method="get">
                        <input name="search" type="text" class="field" placeholder="Поиск..." value="<?=!empty($_REQUEST['search'])?$_REQUEST['search']:'';?>">
                        <div class="icon-place"></div>
                            <button>
                                <img class="icon" src="/resources/img/search-icon.png">
                            </button>
                        
                    </form>
                </div>  
            </div>
        </div>
<div id="category-menu">
    <ul>
        <li class="sunglasses">
            <div class="text">
                СОЛНЦЕЗАЩИТНЫЕ ОЧКИ
                <div class="delimiter"></div>
            </div>
            <div class="opened">
                <div class="header">
                    <div class="text">
                        СОЛНЦЕЗАЩИТНЫЕ ОЧКИ
                    </div>
                    <img class="filter-arrow" src="/resources/img/filter-arrow.png">
                </div>
                <div class="filters">
                    <div data-value="male" class="filter">Мужские</div>
                    <div data-value="female" class="filter">Женские</div>
                    <div data-value="child" class="filter">Детские</div>
                </div>
            </div>
        </li>
        <li class="frames">
            <div class="text">
                ОПРАВЫ
                <div class="delimiter"></div>
            </div>
            <div class="opened">
                <div class="header">
                    <div class="text">
                        ОПРАВЫ
                    </div>
                    <img class="filter-arrow" src="/resources/img/filter-arrow.png">
                </div>
                <div class="filters">
                    <div data-value="male" class="filter">Мужские</div>
                    <div data-value="female" class="filter">Женские</div>
                    <div data-value="child" class="filter">Детские</div>
                </div>
            </div>
        </li>
        <li class="lenses">
            <div class="text">
                КОНТАКТНЫЕ ЛИНЗЫ
                <div class="delimiter"></div>
            </div>
            <div class="opened">
                <div class="header">
                    <div class="text">
                        КОНТАКТНЫЕ ЛИНЗЫ
                    </div>
                    <img class="filter-arrow" src="/resources/img/filter-arrow.png">
                </div>
                <div class="filters">
                    <div data-value="spherical" class="filter">Сферические</div>
                    <div data-value="astigmatic" class="filter">Астигматические</div>
                </div>
            </div>
        </li>
        <li class="related">
            <div class="text">
                СОПУТСТВУЮЩИЕ ТОВАРЫ
                <div class="delimiter"></div>
            </div>
            <div class="opened">
                <div class="header">
                    <div class="text">
                        СОПУТСТВУЮЩИЕ ТОВАРЫ
                    </div>
                    <img class="filter-arrow" src="/resources/img/filter-arrow.png">
                </div>
                <div class="filters">
                    <div data-value="solution" class="filter">Растворы</div>
                    <div data-value="drops" class="filter">Увлажняющие капли</div>
                    <div data-value="set" class="filter">Наборы для линз</div>
                    <div data-value="case" class="filter">Футляры</div>
                    <div data-value="napkin" class="filter">Салфетки</div>
                    <div data-value="spray" class="filter">Спреи</div>
                </div>
            </div>
        </li>  
        <li class="events">
            <div class="text">
                АКЦИИ
            </div>
        </li> 
    </ul>
</div>
        <?php $consultant = \app\models\Users::findOne(1);?>
        <div id="page">
            <?= $content ?>
        </div>
        <div id="hFooter"></div>
        <?php if ($consultant!==null): ?>
        <div class="consultant-panel">
            <span class="can-i-help">Я могу Вам помочь?</span>
            <img class="round" src="/resources/img/consultant/question-round.png">
            <img class="question" src="/resources/img/consultant/question.png">
        </div>
        <?php endif; ?>
    </div>
    <!-- Окно консультанта -->
    
    <?php if ($consultant!==null): ?>
    <div class="consultant">
        
        <div class="header">
            <div class="indicators">
                <img src="/resources/img/consultant/cons_status_red_off.png" class="red-disabled">
                <img src="/resources/img/consultant/cons_status_red_on.png" class="red-enabled">
                <img src="/resources/img/consultant/cons_status_green_off.png" class="green-disabled">
                <img src="/resources/img/consultant/cons_status_green_on.png" class="green-enabled">
            </div>
            <div class="ctrls">
                <img src="/resources/img/consultant/cons_close.png" class="close">
            </div>
        </div>
        <div class="info">
            <div class="photo-place">
                <div class="photo">
                    <img src="/images/consultant/<?=$consultant->photo_name;?>">
                </div>
            </div>
            <div class="name">
                <div class="online">
                    <span class="name-text">
                        Консультант <?=$consultant->realname;?>
                    </span>
                    <br>
                    <span class="descr">
                        Ответим на любой вопрос
                    </span>
                </div>
                <div class="offline">
                    <span class="name-text">
                        Консультант Не в сети
                    </span>
                    <br>
                    <span class="descr">
                        Ответим на любой вопрос, заполните форму
                    </span>
                </div>
            </div> 
        </div>
        <div class="username">
            <div class="online">
                <label>Представьтесь</label>
                <input type="text">
                <img src="/resources/img/consultant/arrow.png">
            </div>
            <div class="offline">
                Оставьте свое сообщение в этой форме, и мы получим его на e-mail и обязательно ответим!
            </div>
        </div>
        <div class="chat-place">
            <div class="online">
                <div class="chat">
                    Окно чата
                </div>
                <div contenteditable="true" class="text-field">
                    
                </div>
            </div>
            <div class="offline">
                <div class="email-fields">
                    <div>
                        <label>Ваше Имя</label>
                        <input type="text">
                    </div>
                    <div>
                        <label>Ваш E-mail</label>
                        <input type="text">
                    </div>
                    <div>
                        <label>Контактный телефон</label>
                        <input type="text">
                    </div>
                </div>
                <div contenteditable="true" class="text-field">
                    Введите Ваше сообщение
                </div>
            </div>
        </div>
        <div class="message">
            <span class="attach-file-text">Прикрепить файл</span>
            <img class="attach-file-image" src="/resources/img/consultant/attach.png">
            <div class="send-message">
                <span>ОТПРАВИТЬ</span>
            </div>
        </div>
        <div class="footer">
            <div class="attach-file"></div>
            <div class="send"></div>
        </div>
    </div>
    <?php endif; ?>
    <footer id="footer">
        <div id="footer-container">
            <div id="footer-links">
                    <ul>
                        <li>
                            <a href="/guestbook">Отзывы</a>
                        </li>
                        <li>
                            <a href="/">Каталог</a>
                        </li>
                        <li>
                            <a href="/news">Новости</a>
                        </li>
                        <li>
                            Акции
                        </li>
                        <li>
                            <a href="/contacts">Контакты</a>
                        </li>
                        <li>
                            <a href="/dio">Доставка и <br>Оплата</a>
                        </li>
                    </ul>
            </div>
            <div id="footer-contact">
                    &copy; Оптика <b>&#34;OPTICa&#34;</b>, <?= date('Y') ?>
                    <br>
                    <br>
                    Телефон +7 (921) 2583698
                    <br>
                    e-mail: optica@optika.ru
            </div>
        </div>
    </footer>
    <?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>