<?php
$column_counter = 1;
if (count($related)>0){
    $cart_arr = array();
    if (!empty($_COOKIE['cart'])){
        $cart_arr = json_decode($_COOKIE['cart'], true);
    }
    foreach ($related as $number => $item): ?>
        <div class="col col-<?=$column_counter; ?>">
            <div class="item" data-id="<?=$item->id;?>">
                <div class="name"><?=$item->brand->name;?> <?=$item->name;?></div>
                <div class="logo">
                    <img src="/images/<?=$item->logo;?>">
                </div>
                <div class="bottom">
                    <div class="price"><?=$item->price;?> руб.</div>
                    <?php 
                    $incart = false;
                    if (in_array($item->id, $cart_arr))
                        $incart = true;
                    ?>
                    <img title="Добавить в корзину" <?php if ($incart): ?>style="display: none;"<?php endif; ?> data-id="<?=$item->id;?>" class="basket" src="/resources/img/basket.png">
                    <a title="Перейти в корзину" <?php if (!$incart): ?>style="display: none;"<?php endif; ?> class="in_cart" href="/cart">В корзине</a>
                </div>
            </div>
        </div>
    <?php
    $column_counter == 3?$column_counter = 1:$column_counter++;
    endforeach;
} else {
    ?>
    <h2 class="no-goods-message">К сожалению, по Вашему запросу товаров не найдено</h2>
    <?php
}
?> 