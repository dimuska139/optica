<?php use yii\widgets\Breadcrumbs; ?>
<?php echo Breadcrumbs::widget([
    'homeLink' => [ 
        'label' => 'ГЛАВНАЯ',
        'url' => Yii::$app->homeUrl,
        'template' => "<span>{link}</span> - ",
    ],
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]); ?>

<div class="item">
    <div class="name_place">
        <div class="name"><?=$product->name; ?></div>
        <div class="views">
            <span><?=$product->views_amount; ?></span>
            <img class="attach-file-image" src="/resources/img/eye.png">
        </div>
    </div>
    <div class="info">
        <div class="left">
            <div class="photos">
                <?php if (!empty($photos)): ?>
                <div class="main">
                    <img src="/images/<?=$photos[0]->photo_name; ?>">
                </div>
                <?php if (count($photos)>1): ?>
                <div class="another">
                    <?php foreach ($photos as $number => $photo): ?> 
                            <div class="col-<?=($number+1);?>">
                                <img src="/images/<?=$photo->photo_name;?>">
                            </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="social">
              <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=04" data-services="google,twitter,vkontakte,facebook,moimir,odnoklassniki"></div>
            </div>
            <div class="back">
                <a href="/related"><img src="/resources/img/arrow-back.png">
                    Вернуться к списку товаров</a>
            </div>
            <div></div>
        </div>
        <div class="right">
            <div class="main">
                <?php if (in_array($product->related->subtype, array('solution', 'spray', 'napkin', 'drops'))): ?>
                <div class="capacity param">
                    <div class="label">Объем:</div>
                    <div class="value">
                    <?php if (!empty($product->related->capacity)): ?>    
                        <?=$product->related->capacity; ?> мл
                    <?php else: ?>
                        -
                    <?php endif; ?>
                    </div>
                </div>
                <div class="manufacturer param">
                    <div class="label">Производитель:</div>
                    <div class="value"><?=$product->manufacturer->name; ?></div>
                </div>
                <?php else: ?>
                <div class="country param">
                    <div class="label">Страна производства:</div>
                    <div class="value"><?=$product->related->country; ?></div>
                </div>
                <?php endif; ?>
            </div>
            <div class="price"><div class="text">ЦЕНА</div><div class="value"><?=$product->price; ?> руб.</div></div>
            <div id="add-to-basket" data-id="<?=$product->id; ?>">КУПИТЬ СЕЙЧАС</div>
        </div>
    </div>
</div>
<?php if (!empty($similar)): ?>
    <div class="similar">
        <div class="head">ПОХОЖИЕ ТОВАРЫ</div>
        <div class="body">
            <?php
                $column_counter = 1;
                foreach ($similar as $number => $good): ?> 
                    <div class="col col-<?=($column_counter);?>">
                        <div class="similar-item" data-id="<?=$good->id;?>" data-type="<?=$good->good_type;?>">
                            
                            <div class="logo">
                                <img src="/images/<?=$good->logo;?>">
                            </div>
                            <div class="name">
                                <?=$good->name;?>
                            </div>
                            <div class="price">
                                Цена <span><?=$good->price;?> руб</span>
                            </div>
                        </div>
                        
                    </div>
                <?php
                    if ($column_counter == 6){
                        $column_counter = 1;
                    } else {
                        $column_counter++;
                    }
                ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<?php if (!empty($history)): ?>
    <div class="history">
        <div class="head">ВЫ СМОТРЕЛИ</div>
        <div class="body">
            <?php
                $column_counter = 1;
                foreach ($history as $number => $good): ?> 
                    <div class="col col-<?=($column_counter);?>">
                        <div class="history-item" data-id="<?=$good->id;?>" data-type="<?=$good->good_type;?>">
                            
                            <div class="logo">
                                <img src="/images/<?=$good->logo;?>">
                            </div>
                            <div class="name">
                                <?=$good->name;?>
                            </div>
                            <div class="price">
                                Цена <span><?=$good->price;?> руб</span>
                            </div>
                        </div>
                        
                    </div>
                <?php
                    if ($column_counter == 6){
                        $column_counter = 1;
                    } else {
                        $column_counter++;
                    }
                ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>