<?php use yii\widgets\Breadcrumbs; ?>
<?php echo \Yii::$app->view->renderFile('@app/views/site/slider.php'); ?>
<div class="related-page"> 
    <div class="search-params">
        <div class="title">ПАРАМЕТРЫ ПОИСКА</div>
        <div class="btn new-filters" id="new-filters">НОВИНКИ</div>
        <div class="optica_select_slider price-filter" id="price-filter">
            <div class="header">
                <div class="text">
                    ЦЕНА, руб
                </div>
                <div class="arrow-place">
                    <img class="arrow" src="./resources/img/arrow-down.png">
                </div>
            </div>
            <div class="slider-place">
                <div class="slider"></div>
                <div class="from">500 руб</div>
                <div class="to">2000 руб</div>
            </div>
        </div>
        <div class="search-title">ПРОИЗВОДИТЕЛЬ</div>
        <div class="optica_select_panel manufacturers-filter" id="manufacturers-filter">
            <?php foreach ($manufacturers as $number => $manufacturer): ?>
            <div data-value="<?=$manufacturer->id;?>"><?=$manufacturer->name;?></div>
            <?php endforeach; ?>
        </div>
        <div class="btn clear-filters" id="clear-filters">СБРОСИТЬ</div>
    </div>
    <div class="related-place">
        <?php echo Breadcrumbs::widget([
                'homeLink' => [ 
                    'label' => 'ГЛАВНАЯ',
                    'url' => Yii::$app->homeUrl,
                    'template' => "<span>{link}</span> - ", // template for this link only
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>
        <div class="title">СОПУТСТВУЮЩИЕ ТОВАРЫ</div>
        <div class="filters">
            <div class="buttons-group subtype-buttons">
                <div class="col col-1">
                    <div data-value="solution" class="btn<?=(isset($_GET['subtype'])&&strcmp($_GET['subtype'],'solution')==0)?' pressed':'';?>">РАСТВОРЫ</div>
                </div>
                <div class="col col-2">
                    <div data-value="drops" class="btn<?=(isset($_GET['subtype'])&&strcmp($_GET['subtype'],'drops')==0)?' pressed':'';?>">УВЛАЖНЯЮЩИЕ КАПЛИ</div>
                </div>
                <div class="col col-3">
                    <div data-value="set" class="btn<?=(isset($_GET['subtype'])&&strcmp($_GET['subtype'],'set')==0)?' pressed':'';?>">НАБОРЫ ДЛЯ ЛИНЗ</div>
                </div>
                <div class="col col-1">
                    <div data-value="case" class="btn<?=(isset($_GET['subtype'])&&strcmp($_GET['subtype'],'case')==0)?' pressed':'';?>">ФУТЛЯРЫ</div>
                </div>
                <div class="col col-2">
                    <div data-value="napkin" class="btn<?=(isset($_GET['subtype'])&&strcmp($_GET['subtype'],'napkin')==0)?' pressed':'';?>">САЛФЕТКИ</div>
                </div>
                <div class="col col-3">
                    <div data-value="spray" class="btn<?=(isset($_GET['subtype'])&&strcmp($_GET['subtype'],'spray')==0)?' pressed':'';?>">СПРЕИ</div>
                </div>
            </div>

            <div class="sortby-and-paging">
                <div class="sortby">
                    <span>Отсортировать по</span>
                    <div class="optica_select sortby-filter" id="sortby-filter">
                        <div class="header">
                            <div class="text">
                                ЦЕНА
                            </div>
                            <div class="arrow-place">
                                <img src="./resources/img/arrow-down.png">
                            </div>
                        </div>
                        <div class="list">
                            <?php
                                $first = true;
                                foreach($sorter_by_array AS $key => $sort_param): ?>
                                    <div <?php if ($first) echo 'class="selected" '; $first=false;?>data-value="<?=$key; ?>"><?=$sort_param; ?></div>
                                <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="optica_paginator">
                    <span class="home">начало</span>
                    <span class="prev">пред.</span>
                    <span class="numbers"></span>
                    <span class="next">след.</span>
                    <span class="end">конец</span>
                </div>
            </div>
        </div>
        <div class="content">
            
        </div>
        <div class="optica_paginator bottom">
            <span class="home">начало</span>
            <span class="prev">пред.</span> 
            <span class="numbers"></span>
            <span class="next">след.</span>
            <span class="end">конец</span>
        </div>
    </div>
</div>