<div class="optica_select">
    <div class="header">
        <div class="text">
            ФРУКТЫ
        </div>
        <div class="arrow-place">
            <img src="/resources/img/arrow-down.png">
        </div>
    </div>
    <div class="list">
        <div data-value="apple">Яблоко</div>
        <div data-value="pear">Груша</div>
        <div data-value="orange">Апельсин</div>
        <div data-value="banana">Банан</div>
    </div>
</div>