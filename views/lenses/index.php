<?php use yii\widgets\Breadcrumbs; ?>
<?php echo \Yii::$app->view->renderFile('@app/views/site/slider.php'); ?>
<div class="lenses-page"> 
    <div class="search-params">
        <div class="title">ПАРАМЕТРЫ ПОИСКА</div>
        <div class="btn new-filters" id="new-filters">НОВИНКИ</div>
        <div class="optica_select_slider price-filter" id="price-filter">
            <div class="header">
                <div class="text">
                    ЦЕНА, руб
                </div>
                <div class="arrow-place">
                    <img class="arrow" src="./resources/img/arrow-down.png">
                </div>
            </div>
            <div class="slider-place">
                <div class="slider"></div>
                <div class="from">500 руб</div>
                <div class="to">2000 руб</div>
            </div>
        </div>
        <div class="search-title">БРЕНДЫ</div>
        <div class="optica_select_panel brands-filter" id="brands-filter">
            <?php foreach ($brands as $number => $brand): ?>
            <div data-value="<?=$brand->id;?>"><?=$brand->name;?></div>
            <?php endforeach; ?>
        </div>
        <div class="search-title">ПРОИЗВОДИТЕЛЬ</div>
        <div class="optica_select_panel manufacturers-filter" id="manufacturers-filter">
            <?php foreach ($manufacturers as $number => $manufacturer): ?>
            <div data-value="<?=$manufacturer->id;?>"><?=$manufacturer->name;?></div>
            <?php endforeach; ?>
        </div>
        
        <div class="search-title">СРОК НОШЕНИЯ</div>
        <div class="optica_select wearing-time-filter" id="wearing-time-filter">
            <div class="header">
                <div class="text">
                    выберите срок
                </div>
                <div class="arrow-place">
                    <img src="./resources/img/arrow-down.png">
                </div>
            </div>
            <div class="list">
                <?php foreach ($wearing_time as $number => $time): ?>
                <div data-value="<?=$time->id;?>"><?=$time->time;?></div>
                <?php endforeach; ?>
            </div>
        </div>
        
        <div class="search-title">РЕЖИМ НОШЕНИЯ</div>
        <div class="optica_select wearing-mode-filter" id="wearing-mode-filter">
            <div class="header">
                <div class="text">
                    выберите режим
                </div>
                <div class="arrow-place">
                    <img src="./resources/img/arrow-down.png">
                </div>
            </div>
            <div class="list">
                <?php foreach ($wearing_mode as $number => $mode): ?>
                <div data-value="<?=$mode->id;?>"><?=$mode->mode;?></div>
                <?php endforeach; ?>
            </div>
        </div>
        
        <div class="btn clear-filters" id="clear-filters">СБРОСИТЬ</div>
    </div>
    <div class="lenses-place">
        <?php echo Breadcrumbs::widget([
                'homeLink' => [ 
                    'label' => 'ГЛАВНАЯ',
                    'url' => Yii::$app->homeUrl,
                    'template' => "<span>{link}</span> - ", // template for this link only
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>
        <div class="title">КОНТАКТНЫЕ ЛИНЗЫ</div>
        <div class="filters">
            <div class="buttons-group subtype-buttons">
                <div class="col col-50-1">
                    <div data-value="spherical" class="btn<?=(isset($_GET['subtype'])&&strcmp($_GET['subtype'],'spherical')==0)?' pressed':'';?>">СФЕРИЧЕСКИЕ</div>
                </div>
                <div class="col col-50-2">
                    <div data-value="astigmatic" class="btn<?=(isset($_GET['subtype'])&&strcmp($_GET['subtype'],'astigmatic')==0)?' pressed':'';?>">АСТИГМАТИЧЕСКИЕ</div>
                </div>
            </div>

            <div class="sortby-and-paging">
                <div class="sortby">
                    <span>Отсортировать по</span>
                    <div class="optica_select sortby-filter" id="sortby-filter">
                        <div class="header">
                            <div class="text">
                                ЦЕНА
                            </div>
                            <div class="arrow-place">
                                <img src="./resources/img/arrow-down.png">
                            </div>
                        </div>
                        <div class="list">
                            <?php
                                $first = true;
                                foreach($sorter_by_array AS $key => $sort_param): ?>
                                    <div <?php if ($first) echo 'class="selected" '; $first=false;?>data-value="<?=$key; ?>"><?=$sort_param; ?></div>
                                <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="optica_paginator">
                    <span class="home">начало</span>
                    <span class="prev">пред.</span>
                    <span class="numbers"></span>
                    <span class="next">след.</span>
                    <span class="end">конец</span>
                </div>
            </div>
        </div>
        <div class="content">
            
        </div>
        <div class="optica_paginator bottom">
            <span class="home">начало</span>
            <span class="prev">пред.</span> 
            <span class="numbers"></span>
            <span class="next">след.</span>
            <span class="end">конец</span>
        </div>
    </div>
</div>