<?php use yii\widgets\Breadcrumbs; ?>
<?php echo Breadcrumbs::widget([
    'homeLink' => [ 
        'label' => 'ГЛАВНАЯ',
        'url' => Yii::$app->homeUrl,
        'template' => "<span>{link}</span> - ",
    ],
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]); ?>

<div class="item">
    <div class="name_place">
        <div class="name"><?=$product->name; ?></div>
        <div class="views">
            <span><?=$product->views_amount; ?></span>
            <img class="attach-file-image" src="/resources/img/eye.png">
        </div>
    </div>
    <div class="info">
        <div class="left">
            <div class="photos">
                <?php if (!empty($photos)): ?>
                <div class="main">
                    <img src="/images/<?=$photos[0]->photo_name; ?>">
                </div>
                <?php if (count($photos)>1): ?>
                <div class="another">
                    <?php foreach ($photos as $number => $photo): ?> 
                            <div class="col-<?=($number+1);?>">
                                <img src="/images/<?=$photo->photo_name;?>">
                            </div>
                    <?php endforeach; ?>
                </div>
                <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="social">
              <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=04" data-services="google,twitter,vkontakte,facebook,moimir,odnoklassniki"></div>
            </div>
            <div class="back">
                <a href="/lenses"><img src="/resources/img/arrow-back.png">
                    Вернуться к списку товаров</a>
            </div>
            <div></div>
        </div>
        <div class="right">
            <div class="main">
                <div class="wearingtime param">
                    <div class="label">Срок замены:</div>
                    <div class="value"><?=$product->lenses->wearingtime->time; ?></div>
                </div>
                
                <div class="type param">
                    <div class="label">Тип линз:</div>
                    <div class="value"><?=$type; ?></div>
                </div>
                
                <div class="manufacturer param">
                    <div class="label">Производитель:</div>
                    <div class="value"><?=$product->manufacturer->name; ?></div>
                </div>
                
                <div class="blisters param">
                    <div class="label">Количество блистеров в упаковке:</div>
                    <div class="value"><?=$product->lenses->blisters_amount; ?></div>
                </div>
                
                <div class="wearingmode param">
                    <div class="label">Режим ношения:</div>
                    <div class="value"><?=$product->lenses->wearingmode->mode; ?></div>
                </div>
                
                <div class="material param">
                    <div class="label">Тип материала:</div>
                    <div class="value"><?=$product->lenses->material->name; ?></div>
                </div>
                
                <div class="moisture_content param">
                    <div class="label">Влагосодержание:</div>
                    <div class="value"><?=$product->lenses->moisture_content; ?>%</div>
                </div>
                
                <div class="transmittance_of_oxygen param">
                    <div class="label">Коэффициент пропускания кислорода:</div>
                    <div class="value"><?=$product->lenses->transmittance_of_oxygen; ?> Dk/t</div>
                </div>
            </div>
            <div class="price"><div class="text">ЦЕНА</div><div class="value"><?=$product->price; ?> руб.</div></div>
            <div id="add-to-basket" data-id="<?=$product->id; ?>">КУПИТЬ СЕЙЧАС</div>
        </div>
    </div>
</div>
<?php if (!empty($similar)): ?>
    <div class="similar">
        <div class="head">ПОХОЖИЕ ТОВАРЫ</div>
        <div class="body">
            <?php
                $column_counter = 1;
                foreach ($similar as $number => $good): ?> 
                    <div class="col col-<?=($column_counter);?>">
                        <div class="similar-item" data-id="<?=$good->id;?>" data-type="<?=$good->good_type;?>">
                            
                            <div class="logo">
                                <img src="/images/<?=$good->logo;?>">
                            </div>
                            <div class="name">
                                <?=$good->name;?>
                            </div>
                            <div class="price">
                                Цена <span><?=$good->price;?> руб</span>
                            </div>
                        </div>
                        
                    </div>
                <?php
                    if ($column_counter == 6){
                        $column_counter = 1;
                    } else {
                        $column_counter++;
                    }
                ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<?php if (!empty($history)): ?>
    <div class="history">
        <div class="head">ВЫ СМОТРЕЛИ</div>
        <div class="body">
            <?php
                $column_counter = 1;
                foreach ($history as $number => $good): ?> 
                    <div class="col col-<?=($column_counter);?>">
                        <div class="history-item" data-id="<?=$good->id;?>" data-type="<?=$good->good_type;?>">
                            
                            <div class="logo">
                                <img src="/images/<?=$good->logo;?>">
                            </div>
                            <div class="name">
                                <?=$good->name;?>
                            </div>
                            <div class="price">
                                Цена <span><?=$good->price;?> руб</span>
                            </div>
                        </div>
                        
                    </div>
                <?php
                    if ($column_counter == 6){
                        $column_counter = 1;
                    } else {
                        $column_counter++;
                    }
                ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>