<div class="contacts"> 
    <div class="title">
        <b>КОНТАКТЫ</b>
    </div>
    <div class="moscow-address">
        <b>Адрес салона оптики Москва:</b> М. Тургеневская, Чистые пруды Луков пер. 10    тел. 8(495)236 01 35<br>
        Часы работы: С 10:00 до 19:00. Воскресенье выходной.
    </div>
    <div class="moscow-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1122.2320831006864!2d37.635929187852675!3d55.76780989049196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46b54a6673f80d2b%3A0x29f9fb7b2316ff15!2z0L_QtdGALiDQm9GD0LrQvtCyLCAxMCwg0JzQvtGB0LrQstCwLCAxMDcwNDU!5e0!3m2!1sru!2sru!4v1430249188838" width="800" height="400" frameborder="0" style="border:0">
        </iframe>
    </div>
    <div class="mahachkala-address">
        <b>Адрес салона оптики Махачкала:</b> пр-т Гамзатова, 15    тел. (8722) 67-42-69<br>
        Часы работы: С 8:00 до 21:00. Без выходных
    </div>
    <div class="mahachkala-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d364.8550998345375!2d47.50836086465547!3d42.98162712274384!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0000000000000000%3A0xc67e83bfadd471e6!2z0J7Qv9GC0LjQuiDRgdC40YLQuA!5e0!3m2!1sru!2sru!4v1431377085908" width="800" height="400" frameborder="0" style="border:0"></iframe>
    </div>
</div>