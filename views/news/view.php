<?php use yii\widgets\Breadcrumbs; ?>
<?php echo Breadcrumbs::widget([
    'homeLink' => [ 
        'label' => 'ГЛАВНАЯ',
        'url' => Yii::$app->homeUrl,
        'template' => "<span>{link}</span> - ",
    ],
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]); ?>

<div class="news">
    <div class="title">
        <h1><?=$news->title; ?></h1>
    </div>
    <div class="text">
        <?=$news->text; ?>
    </div>
    <div class="back">
        <a href="/news"><img src="/resources/img/arrow-back.png">
            Вернуться к списку новостей</a>
    </div>
</div>
