<?php
use yii\i18n\Formatter;
$column_counter = 1;
if (count($news)>0){

    foreach ($news as $number => $item): ?>
        <div class="news">
            <div class="logo">
                <img src="<?=$item->logo;?>">
            </div>
            <div class="info">
                <div class="title">
                    <?=mb_strtoupper($item->title); ?>
                </div>
                <div class="text">
                    <?php $notagstext = strip_tags($item->text);
                        $text = strlen($notagstext)>200?mb_strimwidth($notagstext, 0, 200).'...':$notagstext;
                    ?>
                    <?=$text;?>
                </div>
                <span data-id="<?=$item->id; ?>" class="more">подробнее...</span>
            </div>
        </div>
    <?php
    endforeach;
} else {
    ?>
    <h2 class="no-goods-message">К сожалению, в данный момент новости на сайте отсутствуют</h2>
    <?php
}
?> 