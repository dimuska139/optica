<?php use yii\widgets\Breadcrumbs; ?>
<?php echo Breadcrumbs::widget([
    'homeLink' => [ 
        'label' => 'ГЛАВНАЯ',
        'url' => Yii::$app->homeUrl,
        'template' => "<span>{link}</span> - ",
    ],
    'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
]); ?>

<div class="popup" <?php if (!$show_popup): ?>style="display: none;"<?php endif; ?>>
    <div class="inner">
        <img class="backing" src="/resources/img/popup_window.png">
        <img class="close-button" src="/resources/img/popup-close.png">
        <div class="left">
            <img class="doctor" src="/resources/img/doctor.png">
            <img class="papers" src="/resources/img/papers.png">
            <img class="question" src="/resources/img/question.png">
        </div>
        <div class="right">
            <div class="title">
                КУПИТЬ С ЛИНЗАМИ ПО РЕЦЕПТУ
            </div>
            <div class="offer">
                Предлагаем Вам сразу приобрести к<br>
                вашей новой оправе Линзы по вашему рецепту. У нас<br>
                широкий выбор линз разной ценовой категории.<br>
            </div>
            <div class="profit">
                <div class="line">
                    <div class="checkbox">
                        <img src="/resources/img/checkbox-active.png">
                    </div>
                    <div class="label">Выбор материала</div>
                </div>
                <div class="line">
                    <div class="checkbox">
                        <img src="/resources/img/checkbox-active.png">
                    </div>
                    <div class="label">Только качественные линзы</div>
                </div>
                <div class="line">
                    <div class="checkbox">
                        <img src="/resources/img/checkbox-active.png">
                    </div>
                    <div class="label">Год гарантии</div>
                </div>
                <div class="line">
                    <div class="checkbox">
                        <img src="/resources/img/checkbox-active.png">
                    </div>
                    <div class="label">Большой выбор покрытия: SUPRA, TRIO, CRIZAL ALIZE+, CRIZAL FORTE, OPTIFOG</div>
                </div>
                <div class="line">
                    <div class="checkbox">
                        <img src="/resources/img/checkbox-active.png">
                    </div>
                    <div class="label">Разновидность: антифары, компьютерные, окраска в цвет, фотохромные.</div>
                </div>
            </div>
            <div class="price">
                Стоимость очковых линз от <span>800</span> рублей!
            </div>
            <input type="button" value="КУПИТЬ ТОЛЬКО ОПРАВУ" class="only-frame">
            <input type="button" value="ДОБАВИТЬ ЛИНЗЫ" class="add-lenses">
        </div>
    </div>
</div>


<div class="item">
    <div class="name_place">
        <div class="name"><?=$product->name; ?></div>
        <div class="views">
            <span><?=$product->views_amount; ?></span>
            <img class="attach-file-image" src="/resources/img/eye.png">
        </div>
    </div>
    <div class="info">
        <div class="left">
            <div class="photos">
                <?php if (!empty($photos)): ?>
                    <div class="main">
                        <img src="/images/<?=$photos[0]->photo_name; ?>">
                    </div>
                    <?php if (count($photos)>1): ?>
                    <div class="another">
                        <?php foreach ($photos as $number => $photo): ?> 
                                <div class="col-<?=($number+1);?>">
                                    <img src="/images/<?=$photo->photo_name;?>">
                                </div>
                        <?php endforeach; ?>
                    </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
            <div class="social">
              <div class="pluso" data-background="transparent" data-options="medium,round,line,horizontal,nocounter,theme=04" data-services="google,twitter,vkontakte,facebook,moimir,odnoklassniki"></div>
            </div>
            <div class="back">
                <a href="/frames"><img src="/resources/img/arrow-back.png">
                    Вернуться к списку товаров</a>
            </div>
            <div></div>
        </div>
        <div class="right">
            <div class="main">
                <div class="brand_name param">
                    <div class="label">Бренд:</div>
                    <div class="value"><?=$product->brand->name; ?></div>
                </div>
                <div class="sku param">
                    <div class="label">Код модели:</div>
                    <div class="value"><?=$product->sku; ?></div>
                </div>
                <div class="manufacturer param">
                    <div class="label">Производитель:</div>
                    <div class="value"><?=$product->manufacturer->name; ?></div>
                </div>
                <div class="material param">
                    <div class="label">Материал оправы:</div>
                    <div class="value"><?=$product->frames->framematerial->name; ?></div>
                </div>
                <div class="frame-color param">
                    <div class="label">Цвет оправы:</div>
                    <div class="value"><?=$product->frames->framecolor->name; ?></div>
                </div>
                <div class="sex param">
                    <div class="label">Пол:</div>
                    <div class="value"><?=$subtype; ?></div>
                </div>
            </div>
            <div class="price"><div class="text">ЦЕНА</div><div class="value"><?=$product->price; ?> руб.</div></div>
            <div id="add-to-basket" data-id="<?=$product->id; ?>">КУПИТЬ СЕЙЧАС</div>
        </div>
    </div>
</div>

<?php if (!empty($similar)): ?>
    <div class="similar">
        <div class="head">ПОХОЖИЕ ТОВАРЫ</div>
        <div class="body">
            <?php
                $column_counter = 1;
                foreach ($similar as $number => $good): ?> 
                    <div class="col col-<?=($column_counter);?>">
                        <div class="similar-item" data-id="<?=$good->id;?>" data-type="<?=$good->good_type;?>"> 
                            <div class="logo">
                                <img src="/images/<?=$good->logo;?>">
                            </div>
                            <div class="name">
                                <?=$good->name;?>
                            </div>
                            <div class="price">
                                Цена <span><?=$good->price;?> руб</span>
                            </div>
                        </div>
                        
                    </div>
                <?php
                    if ($column_counter == 6){
                        $column_counter = 1;
                    } else {
                        $column_counter++;
                    }
                ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>

<?php if (!empty($history)): ?>
    <div class="history">
        <div class="head">ВЫ СМОТРЕЛИ</div>
        <div class="body">
            <?php
                $column_counter = 1;
                foreach ($history as $number => $good): ?> 
                    <div class="col col-<?=($column_counter);?>">
                        <div class="history-item" data-id="<?=$good->id;?>" data-type="<?=$good->good_type;?>">
                            
                            <div class="logo">
                                <img src="/images/<?=$good->logo;?>">
                            </div>
                            <div class="name">
                                <?=$good->name;?>
                            </div>
                            <div class="price">
                                Цена <span><?=$good->price;?> руб</span>
                            </div>
                        </div>
                        
                    </div>
                <?php
                    if ($column_counter == 6){
                        $column_counter = 1;
                    } else {
                        $column_counter++;
                    }
                ?>
            <?php endforeach; ?>
        </div>
    </div>
<?php endif; ?>