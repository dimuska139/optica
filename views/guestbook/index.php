<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    use yii\captcha\Captcha;
?>
<div class="guestbook"> 
    <div class="title">
        <b>ОТЗЫВЫ</b>
    </div>
    <?php if (!$comment_sent): ?>
        <?php
            $form = ActiveForm::begin([
                'id' => 'guestbook-form',
            ]);
        ?>
        <?= $form->field($comment_form, 'subject') ?>
        <?= $form->field($comment_form, 'name') ?>
        <?= $form->field($comment_form, 'email') ?>
        <?= $form->field($comment_form, 'comment')->textArea(['rows' => '6']) ?>
        <?= $form->field($comment_form, 'verifyCode')->widget(Captcha::className()) ?>
        <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'btn']) ?>
        <?php ActiveForm::end(); ?>
    <?php else:  ?>
        <span class="comment-sent-message">
            Спасибо за Ваш отзыв! Он будет опубликован после проверки администратором
        </span>
    <?php endif; ?>
    <div class="comments">
    <?php if (count($comments)>0): ?>
        <?php foreach ($comments as $number => $comment): ?>
            <div class="comment">
                <div class="head">
                    <span class="text">
                        <?=mb_strtoupper(Html::encode($comment->name), 'utf-8'); ?>, <?=mb_strtoupper(Html::encode($comment->subject), 'utf-8'); ?>
                    </span>
                </div>
                <div class="body">
                    <span class="text">
                        <?=Html::encode($comment->comment); ?>
                    </span>
                </div>
                <div class="date">
                    <?php $date = new DateTime($comment->create_date); ?>
                    <?=date_format($date, "d.m.Y"); ?>
                </div>
            </div>
        <?php endforeach; ?>   
    <?php else: ?>
        <span class="no-comments-message">
            Пока что нет опубликованных отзывов. Будьте первыми!
        </span>
    <?php endif; ?>
    </div>
</div>

                