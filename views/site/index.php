<?php echo \Yii::$app->view->renderFile('@app/views/site/slider.php'); ?>
<?php if (count($new)>0):
    $column_counter = 1;
?>
<div id="novelty">
    <div class="title">НОВИНКИ</div>
    <?php foreach ($new as $number => $item): ?>
    <div class="col col-<?=$column_counter; ?>">
        <div class="item" data-id="<?=$item->id;?>" data-type="<?=$item->good_type;?>">
            <div class="logo">
                <img src="/images/<?=$item->logo;?>">
            </div>
            <div class="name"><?=$item->name;?></div>
            <div class="price"><?=$item->price;?> руб.</div>
        </div>
    </div>
    <?php
        $column_counter == 6?$column_counter = 1:$column_counter++;
    ?>
    <?php    endforeach; ?>
</div>
<?php endif; ?>
<div id="special-offer">
    <div class="title">СПЕЦИАЛЬНОЕ ПРЕДЛОЖЕНИЕ</div>
    <?php for ($i=0;$i<6;$i++): ?>
        <div class="item">
            <img src="./images/logo6.png">
            <div class="name">
                Ray Ban
            </div>
            <div class="price">
                10 000 руб.
            </div>
        </div>
    <?php endfor; ?>
</div>
