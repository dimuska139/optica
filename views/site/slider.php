<div class="slider">
    <img class="slider-background" src="./resources/img/slider-background.png">
    <img class="slider-glasses" src="./resources/img/glasses.png">
    <ul>
        <li><img src="./resources/slider/slider_image_1.png" alt=""></li>
        <li><img src="./resources/slider/slider_image_2.png" alt=""></li>
        <li><img src="./resources/slider/slider_image_3.png" alt=""></li>
        <li><img src="./resources/slider/slider_image_4.png" alt=""></li>
        <li><img src="./resources/slider/slider_image_5.png" alt=""></li>
        <li><img src="./resources/slider/slider_image_6.png" alt=""></li>
    </ul>
</div>