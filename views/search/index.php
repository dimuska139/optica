<?php use yii\widgets\Breadcrumbs; ?>
<?php echo \Yii::$app->view->renderFile('@app/views/site/slider.php'); ?>
<div class="search-page"> 
    <div class="search-params">
        <div class="title">ПАРАМЕТРЫ ПОИСКА</div>
        <div class="optica_select_panel good-types" id="good-types">
            <div data-value="1">СОЛНЦЕЗАЩИТНЫЕ ОЧКИ</div>
            <div data-value="2">ОПРАВЫ</div>
            <div data-value="3">КОНТАКТНЫЕ ЛИНЗЫ</div>
            <div data-value="4">СОПУТСТВУЮЩИЕ ТОВАРЫ</div>
        </div>
        <div class="optica_select price-filter" id="price-filter">
            <div class="header">
                <div class="text">
                    ЦЕНА, руб
                </div>
                <div class="arrow-place">
                    <img src="./resources/img/arrow-down.png">
                </div>
            </div>
            <div class="list">
                <div data-min="0" data-max="3000" data-value="-3">до 3000</div>
                <div data-min="3000" data-max="6000" data-value="3-6">от 3000 до 6000</div>
                <div data-min="6000" data-max="9000" data-value="6-9">от 6000 до 9000</div>
                <div data-min="9000" data-max="12000" data-value="9-12">от 9000 до 12000</div>
                <div data-min="12000" data-max="15000" data-value="12-15">от 12000 до 15000</div>
            </div>
        </div>
        <div class="search-title">БРЕНДЫ</div>
        <div class="optica_select_panel brands-filter" id="brands-filter">
            <?php foreach ($brands as $number => $brand): ?>
            <div data-value="<?=$brand->id;?>"><?=$brand->name;?></div>
            <?php endforeach; ?>
        </div>
        <div class="search-title">ФОРМА</div>
        <div class="optica_select_panel forms-filter" id="forms-filter">
            <?php foreach ($forms as $number => $form): ?>
            <div data-value="<?=$form->id;?>"><img src="./resources/img/<?=$form->img_url;?>"><span><?=$form->name;?></span></div>
            <?php endforeach; ?>
        </div>
        <div class="search-title">ТИП ОПРАВЫ</div>
        <div class="optica_select_panel frame-filter" id="frame-filter">
            <?php foreach ($frame_types as $number => $frame_type): ?>
            <div data-value="<?=$frame_type->id;?>"><img src="./resources/img/<?=$frame_type->img_url;?>"><span><?=$frame_type->name;?></span></div>
            <?php endforeach; ?>
        </div>
        <div class="search-title">МАТЕРИАЛ ОПРАВЫ</div>
        <div class="optica_select_panel material-filter" id="material-filter">
            <?php foreach ($frame_materials as $number => $frame_material): ?>
            <div data-value="<?=$frame_material->id;?>"><?=$frame_material->name;?></div>
            <?php endforeach; ?>
        </div>
        <div class="search-title">ЦВЕТ ОПРАВЫ</div>
        <div class="optica_select frame-color-filter" id="frame-color-filter">
            <div class="header">
                <div class="text">
                    выберите цвет
                </div>
                <div class="arrow-place">
                    <img src="./resources/img/arrow-down.png">
                </div>
            </div>
            <div class="list">
                <?php foreach ($frame_colors as $number => $frame_color): ?>
                <div data-value="<?=$frame_color->id;?>"><?=$frame_color->name;?></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="search-title">ЦВЕТ ЛИНЗ</div>
        <div class="optica_select lens-color-filter" id="lens-color-filter">
            <div class="header">
                <div class="text">
                    выберите цвет
                </div>
                <div class="arrow-place">
                    <img src="./resources/img/arrow-down.png">
                </div>
            </div>
            <div class="list">
                <?php foreach ($lens_colors as $number => $lens_color): ?>
                <div data-value="<?=$lens_color->id;?>"><?=$lens_color->name;?></div>
                <?php endforeach; ?>
            </div>
        </div>
        <div class="btn clear-filters" id="clear-filters">СБРОСИТЬ</div>
    </div>
    <div class="search-place">
        <?php echo Breadcrumbs::widget([
                'homeLink' => [ 
                    'label' => 'ГЛАВНАЯ',
                    'url' => Yii::$app->homeUrl,
                    'template' => "<span>{link}</span> - ", // template for this link only
                ],
                'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
            ]); ?>
        <div class="filters">
            <div class="sortby-and-paging">
                <div class="sortby">
                    <span>Отсортировать по</span>
                    <div class="optica_select sortby-filter" id="sortby-filter">
                        <div class="header">
                            <div class="text">
                                ЦЕНА
                            </div>
                            <div class="arrow-place">
                                <img src="./resources/img/arrow-down.png">
                            </div>
                        </div>
                        <div class="list">
                            <?php
                                $first = true;
                                foreach($sorter_by_array AS $key => $sort_param): ?>
                                    <div <?php if ($first) echo 'class="selected" '; $first=false;?>data-value="<?=$key; ?>"><?=$sort_param; ?></div>
                                <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <div class="optica_paginator">
                    <span class="home">начало</span>
                    <span class="prev">пред.</span>
                    <span class="numbers"></span>
                    <span class="next">след.</span>
                    <span class="end">конец</span>
                </div>
            </div>
        </div>
        <div class="content">
            
        </div>
        <div class="optica_paginator bottom">
            <span class="home">начало</span>
            <span class="prev">пред.</span> 
            <span class="numbers"></span>
            <span class="next">след.</span>
            <span class="end">конец</span>
        </div>
    </div>
</div>