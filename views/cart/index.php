<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>
<div class="cart-place">
    <div class="title">ВАША КОРЗИНА</div>
    <?php
        $form = ActiveForm::begin([
            'id' => 'cart-form',
        ]);
    ?>
    <div class="cart-container"<?php if (count($cart_list)==0): ?> style="display: none;"<?php endif; ?>>
        <div class="cart-table-place">
            <table class="cart-table" border="1">
                <tr class="header">
                    <td>
                        ИЗОБРАЖЕНИЕ
                    </td>
                    <td>
                        КРАТКОЕ ОПИСАНИЕ
                    </td>
                    <td>
                        КОЛИЧЕСТВО
                    </td>
                    <td>
                        УДАЛИТЬ
                    </td>
                    <td>
                        СТОИМОСТЬ
                    </td>
                </tr>
                <?php foreach ($cart_list as $number => $item): ?>
                <tr id="product-<?=$item->id; ?>" data-id="<?=$item->id; ?>" data-price="<?=$item->price; ?>">
                    <td align="center">
                        <img class="logo" src="/images/<?=$item->logo;?>">
                    </td>
                    <td align="center">
                        <?=$item->name; ?>
                    </td>
                    <td align="center" class="amount-place">
                        <div class="amount" data-id="<?=$item->id; ?>">
                            <div class="less">-</div>
                            <div class="display">
                                <div class="info">
                                    <?=$amount_order[$number]; ?>
                                </div>
                            </div>
                            <div class="more">+</div>
                            <input id="amount_value_<?=$item->id; ?>" name="amount_value_<?=$item->id; ?>" type="hidden" value="<?=$amount_order[$number]; ?>" class="amount_value">
                        </div>                        
                    </td>
                    <td align="center">
                        <div class="delete">
                            <img src="/resources/img/delete_х.png">
                        </div>
                    </td>
                    <td class="product-cost" align="center">
                        <?=$item->price; ?>
                        <input type="hidden" class="price_value" id="price_value_<?=$item->id; ?>" value="<?=$item->price; ?>">
                    </td>
                </tr>
                <?php endforeach; ?>
            </table>
        </div>
        <div class="profit">
            При заказе на сумму более 3 000 рублей - доставка бесплатная.
        </div>
        <div class="promo-and-sum">
            <div class="promo">
                <div class="label">Введите промо-КОД</div>
                <input type="text" class="promo-text">
                <img src="/resources/img/ok.png">
            </div>
            <div class="sum">
                <div class="label">ИТОГО:</div>
                <div class="value"></div>
            </div>
        </div>
        
        <div class="delivery">
            <div class="title">ДОСТАВКА</div>
            <?= $form->field($cart_form, 'delivery')->radioList(
                    $delivery_array, [
                        'item' => function ($index, $label, $name, $checked, $value) {
                            $check = $checked ? ' checked="checked"' : '';
                            return '<input type="radio" name="'.$name.'" value="'.$value.'" '.$check.'><span>'.$label.'</span><br><br>';
                        }])->label(false); ?>

            <div class="info">
                Доставка по Москве в пределах МКАД на сумму менее 3000 руб. - 200 рублей<br>
                Доставка по Москве в пределах МКАД на сумму более 3000 руб. - БЕСПЛАТНО<br>
                Доставка в другие регионы России не зависимо от общей суммы - 490 рублей
            </div>
            <div class="sum-with-delivery">
                <div class="label">ИТОГО С ДОСТАВКОЙ:</div>
                <div class="value"></div>
            </div>
        </div>
        <div class="payment">
            <div class="title">ОПЛАТА</div>
            <?= $form->field($cart_form, 'payment')->radioList(
                    $payment_array, [
                        'item' => function ($index, $label, $name, $checked, $value) {
                            $check = $checked ? ' checked="checked"' : '';
                            return '<input type="radio" name="'.$name.'" value="'.$value.'" '.$check.'><span>'.$label.'</span><br><br>';
                        }])->label(false); ?>
        </div>
        <div class="user-info">
            <div class="title">АДРЕС ДОСТАВКИ</div>
            <?= $form->field($cart_form, 'name') ?>
            <?= $form->field($cart_form, 'surname') ?>
            <?= $form->field($cart_form, 'email') ?>
            <?= $form->field($cart_form, 'retypeEmail') ?>
            <?= $form->field($cart_form, 'phone') ?>
            <?= $form->field($cart_form, 'address') ?>
            <?= $form->field($cart_form, 'index') ?>
            <?= $form->field($cart_form, 'city') ?>
            <?= $form->field($cart_form, 'region') ?>
        </div>
        <?= Html::submitButton('ОТПРАВИТЬ', ['class' => 'buy']) ?>      
    </div>
    <?php ActiveForm::end(); ?>
    <div class="cart-empty"<?php if (count($cart_list)!=0): ?> style="display: none;"<?php endif; ?>>
        Ваша корзина пуста. Начните с <a href="/">главной страницы</a> или воспользуйтесь поиском.
    </div>
</div>