$(document).ready(function(){
    fillCostFields();
});

// Заполнение полуй со стоимостью с доставкой и без учета доставки
function fillCostFields(){
    var cost = countCost();
    var delivery = countCostDelivery();
    $('.sum .value').html(cost);
    $('.sum-with-delivery .value').html(cost+delivery)
}


// Удалить из корзины
$('.delete').on('click', function(){
    var id = $(this).parent('td').parent('tr').data('id');
    fromCart(id);
    $('#product-'+id).remove();
    if (cartAmount()==0){
        $('.cart-container').hide();
        $('.cart-empty').show();
    }
});

// Увеличить количество единиц определенного товара
$('.more').on('click', function(){
    var amount = parseInt($(this).parent('.amount').find('.display .info').html());
    var id = $(this).parent('.amount').data('id');
    amount++;
    $(this).parent('.amount').find('.display .info').html(amount);
    $('#amount_value_'+id).val(amount);
    updateCartAmount(id, amount);
    fillCostFields();
});


// Уменьшить количество единиц определенного товара
$('.less').on('click', function(){
    var amount = parseInt($(this).parent('.amount').find('.display .info').html());
    var id = $(this).parent('.amount').data('id');
    amount--;
    if (amount>0){
        $(this).parent('.amount').find('.display .info').html(amount);
        $('#amount_value_'+id).val(amount);
        updateCartAmount(id, amount);
    } else { // Если количество было 1, то просто удаляем из корзины
        fromCart(id);
        $('#product-'+id).remove();
        if (cartAmount() == 0){
            $('.cart-container').hide();
            $('.cart-empty').show();
        }
    }
    fillCostFields();
});

// Считаем стоимость
function countCost(){
    var sum = 0;
    $('.amount_value').each(function(){
        var id = $(this).parent('.amount').data('id');
        sum += parseInt($('#amount_value_'+id).val())*parseInt($('#price_value_'+id).val());
    });
    return sum;
}

// Считаем стоимость с доставкой
function countCostDelivery(){
    switch($('#cartform-delivery input:checked').val()){
        case 'himself':
            return 0;
            break;
        case 'moscow':
            if (countCost()<=3000)
                return 200;
            else
                return 0;
            break;
        case 'russia':
            return 490;
            break;
    }
}

// Изменение варианта доставки
$('#cartform-delivery input').on('change', function(){
    fillCostFields();
});