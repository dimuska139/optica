/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var consultant = new Consultant();

$(document).ready(function(){
    $('.optica_select').optica_select(); // аналог select
    $('.optica_select_panel').optica_select_panel(); // список с возможностью выбирать несколько строк
    updateCartCounter();
});

$('#category-menu ul .sunglasses').on('click', function(){
    if ($('#category-menu ul .sunglasses .opened').css('display') === 'none'){
        $('#category-menu ul .opened').slideUp();
        $('#category-menu ul .sunglasses .opened').slideDown();
    }
});

$('#category-menu ul .sunglasses .opened .header').on('click', function(){
    $('#category-menu ul .sunglasses .opened').slideUp();
});

$('#category-menu ul .sunglasses .opened .filters .filter').on('click', function(){
    window.location = '/sunglasses?subtype='+$(this).data('value');
});


$('#category-menu ul .related').on('click', function(){
    if ($('#category-menu ul .related .opened').css('display') === 'none'){
        $('#category-menu ul .opened').slideUp();
        $('#category-menu ul .related .opened').slideDown();
    }
});

$('#category-menu ul .related .opened .header').on('click', function(){
    $('#category-menu ul .related .opened').slideUp();
});

$('#category-menu ul .related .opened .filters .filter').on('click', function(){
    window.location = '/related?subtype='+$(this).data('value');
});


$('#category-menu ul .lenses').on('click', function(){
    if ($('#category-menu ul .lenses .opened').css('display') === 'none'){
        $('#category-menu ul .opened').slideUp();
        $('#category-menu ul .lenses .opened').slideDown();
    }
});

$('#category-menu ul .lenses .opened .header').on('click', function(){
    $('#category-menu ul .lenses .opened').slideUp();
});

$('#category-menu ul .lenses .opened .filters .filter').on('click', function(){
    window.location = '/lenses?subtype='+$(this).data('value');
});


$('#category-menu ul .frames').on('click', function(){
    if ($('#category-menu ul .frames .opened').css('display') === 'none'){
        $('#category-menu ul .opened').slideUp();
        $('#category-menu ul .frames .opened').slideDown();
    }
});

$('#category-menu ul .frames .opened .header').on('click', function(){
    $('#category-menu ul .frames .opened').slideUp();
});

$('#category-menu ul .frames .opened .filters .filter').on('click', function(){
    window.location = '/frames?subtype='+$(this).data('value');
});

$('.btn').on('click', function(){
    !$(this).hasClass('pressed')?$(this).addClass('pressed'):$(this).removeClass('pressed');
    loadData();
});

// Добавление товара в корзину
function toCart(id){
    var cart = $.cookie('cart');
    var cart_arr = [];
    if (cart != null)
        cart_arr = JSON.parse(cart);
    
    var cart_amount = $.cookie('cart_amount');
    var cart_amount_arr = [];
    if (cart_amount != null)
        cart_amount_arr = JSON.parse(cart_amount);
    
    
    var exists = false;
    for (var i=0;i<cart_arr.length;i++){
        if (cart_arr[i]==id)
            exists = true;
    }
    if (!exists){
        cart_arr.push(id);
        var cart_json = JSON.stringify(cart_arr);
        $.cookie('cart', cart_json, { path: '/'});
        
        cart_amount_arr.push(1);
        var cart_amount_json = JSON.stringify(cart_amount_arr);
        $.cookie('cart_amount', cart_amount_json, { path: '/'});
    }
    updateCartCounter();
}

// Удаление товара из корзины
function fromCart(id){
    var cart = $.cookie('cart');
    var cart_amount = $.cookie('cart_amount');
    var cart_arr = [];
    if (cart != null)
        cart_arr = JSON.parse(cart);
    var cart_amount_arr = [];
    if (cart_amount != null)
        cart_amount_arr = JSON.parse(cart_amount);
    for (var i=0;i<cart_arr.length;i++){
        if (cart_arr[i]==id){
            cart_arr.splice(i,1);
            cart_amount_arr.splice(i,1);
        }
    }
    var cart_json = JSON.stringify(cart_arr);
    $.cookie('cart', cart_json);
    var cart_amount_json = JSON.stringify(cart_amount_arr);
    $.cookie('cart_amount', cart_amount_json);
    updateCartCounter();
}

// Обновление количества определенного товара в корзине
function updateCartAmount(id, new_amount){
    var cart = $.cookie('cart');
    var cart_amount = $.cookie('cart_amount');
    var cart_arr = [];
    if (cart != null)
        cart_arr = JSON.parse(cart);
    var cart_amount_arr = [];
    if (cart_amount != null)
        cart_amount_arr = JSON.parse(cart_amount);
    for (var i=0;i<cart_arr.length;i++){
        if (cart_arr[i]==id){
            cart_amount_arr[i] = new_amount;
        }
    }
    var cart_amount_json = JSON.stringify(cart_amount_arr);
    $.cookie('cart_amount', cart_amount_json);
}

// Обновление индикатора корзины вверху страницы
function updateCartCounter(){
    var cart_length = cartAmount();
    if (cart_length>0){
        $('#basket .counter span').html(cart_length);
        $('#basket .field span').html('В корзине:');
    } else {
        $('#basket .counter span').html('');
        $('#basket .field span').html('Корзина пуста');
    }
}

function cartAmount(){
    var cart = $.cookie('cart');
    var cart_arr = [];
    if (cart != null)
        cart_arr = JSON.parse(cart);
    return cart_arr.length;
}

/*$('#basket').on('click', function(){
    document.location.href = "/cart"; 
});*/