(function( $ ){
// Фильтр-панель с возможностью выбора нескольких элементов
    var methods = {
        init : function(options) {
            return this.each(function(){
                $(this).on('click',function(event){
                    var value = event.target.dataset.value;
                    if (typeof value === 'undefined')
                        value = $(event.target).parent('div').data('value');
                    $(this).data('selected', value);
                    var selected_elem = $(this).children('[data-value="'+value+'"]');
                    if (selected_elem.hasClass('selected'))
                        selected_elem.removeClass('selected')
                    else
                        selected_elem.addClass('selected');
                    loadData();
                });
            });
        },
        select: function(val){
            var selected_elem = $(this).children('[data-value="'+val+'"]');
            selected_elem.addClass('selected');
        },
        deselect: function(val){
            var selected_elem = $(this).children('[data-value="'+val+'"]');
            selected_elem.removeClass('selected');
        },
    };
  
    $.fn.optica_select_panel = function( method ) {
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.optica_select_panel' );
        }
    };
})( jQuery );