/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var page_size = 10; // количество элементов на странице
var page_number = 1;
var count = 0;

$('.content').on('click', '.more', function(){
    document.location.href = "/news/view/?id="+$(this).data('id');
});

function loadData(page_changed){
    if (!page_changed)
        page_number = 1;
    
    var filters = new Object();
    filters.page = page_number;
    
    $.ajax({
        type: "get",
        url: "/news/getlist/",
        dataType: "json",
        data: filters,
        beforeSend: function(){
        },
        complete: function(){
        },
        success: function(response){
            $('.content').html(response.html);
            count = response.count;
            fill_paginator();
        }
    });
}

loadData();
