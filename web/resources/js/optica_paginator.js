// Постраничная навигация для сайта

// Подсчет количества страниц
function pageAmount(count){
    if (count%page_size==0)
        return count/page_size;
    else
        return Math.floor(count/page_size)+1;
}
// Отрисовка номеров страниц
function fillNumbers(min, max){
    if (Math.floor(count/page_size)!=0){
        for (var i=min;i<=max;i++){
            $('.optica_paginator .numbers').append('<span data-value="'+i+'" class="'+((i==page_number)?'selected ':'')+'page page_'+i+'">'+i+'</span>'+((i!=max)?', ':' '));
        }
    }
}

function fill_paginator(){
    var page_amount = pageAmount(count);
    
    $('.optica_paginator .numbers').html('');
    if (page_amount<10){
        fillNumbers(1, page_amount);
        $('.optica_paginator .home').hide();
        $('.optica_paginator .end').hide();
        $('.optica_paginator .next').hide();
        $('.optica_paginator .prev').hide();
    } else {
        if (page_number<5){
            fillNumbers(1, 5);
            $('.optica_paginator .numbers').append('....................');
            $('.optica_paginator .home').hide();
            $('.optica_paginator .end').show();
            $('.optica_paginator .next').show();
            $('.optica_paginator .prev').hide();
        } else {
            if (page_number+4<=page_amount){
                $('.optica_paginator .numbers').append('..........');
                fillNumbers((page_number-2), (page_number+2));
                $('.optica_paginator .numbers').append('..........');
                $('.optica_paginator .home').show();
                $('.optica_paginator .end').show();
                $('.optica_paginator .next').show();
                $('.optica_paginator .prev').show();
            } else {
                $('.optica_paginator .numbers').append('....................');
                fillNumbers((page_amount-5), page_amount);
                $('.optica_paginator .home').show();
                $('.optica_paginator .end').hide();
                $('.optica_paginator .next').hide();
                $('.optica_paginator .prev').show();
            }
        }
    }
    
    
    $('.optica_paginator .numbers .page').on('click', function(){
        var current_page = $(this).data('value');
        page_number = current_page;
        loadData('page_changed');
    });
}

$('.optica_paginator .end').on('click', function(){
    page_number = pageAmount(count);
    loadData('page_changed');
});

$('.optica_paginator .home').on('click', function(){
    page_number = 1;
    loadData('page_changed');
});

$('.optica_paginator .prev').on('click', function(){
    if (page_number>1){
        page_number--;
        loadData('page_changed');
    }
});

$('.optica_paginator .next').on('click', function(){
    if (page_number<pageAmount(count)){
        page_number++;
        loadData('page_changed');
    }
});