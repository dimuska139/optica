function Consultant(){
    var me = this;
    this.status = 1;
    this.timeout = 1; // Связь с сервером каждую секунду
    this.max_message_length = 385; // Максимальная длина сообщения (в символах)
    this.placeholder = "Введите Ваше сообщение";
    $('.consultant .chat-place .text-field').html(this.placeholder);
    // Показать панель консультанта
    $('.consultant-panel').on('click', function(){
        me.showConsultant();
    });
    // Скрыть панель консультанта
    $('.consultant .close').on('click', function(){
        me.hideConsultant();
    });
    
    // Сделать панель консультанта перетаскиваемой
    $('.consultant').draggable({
        containment: 'window', // Только в пределах окна браузера
        handle: '.header' // Только за шапку окна консультанта
    });
    
    $('.online .text-field, .offline .text-field').on('click', function(){
        
    });
    
    this.checkState(this);
    if ($.cookie('consultant_open')==1){
        me.showConsultant();
    } else {
        $('.consultant-panel').show();
    }
    // Таймер для вызова функции проверки статуса консультанта
    this.timer = setInterval(this.checkState, this.timeout*1000, this);
}

Consultant.prototype = {
    checkState: function(cons){
        // Закомментировано, т.к. бесплатный хостинг не дает так часто обращаться к серверу
     /*   $.ajax({
            type: "get",
            url: "./consultant/getstatus/",
            dataType: "json",
            beforeSend: function(){
            },
            complete: function(){
            },
            success: function(response){
                var old = cons.status; // предыдущий статус
                cons.status = parseInt(response.online);
                if (old != cons.status){ // сменился статус
                    if (cons.status===1)
                        cons.setOnline();
                    else
                        cons.setOffline();
                }
            }
        });*/
    },
    showConsultant: function(){
        $.cookie('consultant_open',1);
        $('.consultant-panel').fadeOut();
        $('.consultant').show();
    },
    hideConsultant: function(){
        $.cookie('consultant_open',0);
        $('.consultant').hide();
        $('.consultant-panel').fadeIn();
    },
    // Смена статуса консультанта на "Онлайн"
    setOnline: function(){
        $('.consultant .indicators .red-disabled').show();
        $('.consultant .indicators .red-enabled').hide();
        $('.consultant .indicators .green-enabled').show();
        $('.consultant .indicators .green-disabled').hide();
        
        $('.consultant .online').show();
        $('.consultant .offline').hide();
        
        $('.consultant .chat-place').css('height', 240);
    },
    // Смена статуса консультанта на "Офлайн"
    setOffline: function(){
        $('.consultant .indicators .red-disabled').hide();
        $('.consultant .indicators .red-enabled').show();
        $('.consultant .indicators .green-enabled').hide();
        $('.consultant .indicators .green-disabled').show();
        
        $('.consultant .online').hide();
        $('.consultant .offline').show();
        
        $('.consultant .chat-place').css('height', 229);
    }
}