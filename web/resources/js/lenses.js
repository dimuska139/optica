$(document).ready(function(){
    $( ".optica_select_slider" ).optica_select_slider({'min': 0, 'max': 3500, 'values': [500, 2000]});
    loadData();
});

var page_size = 30; // количество элементов на странице
var page_number = 1; // номер страницы
var count = 0; // количество товаров данного типа (всего)

// Переход на карточку товаров
$('.content').on('click', '.item', function(){
    document.location.href = "lenses/product/?id="+$(this).data('id');
});

// Очистка фильтров
$('#clear-filters').on('click', function(){
    location.reload();
});

// Добавление в корзину
$('.content').on('click', '.basket', function(){
    $(this).hide();
    $(this).parent('.bottom').find('.in_cart').show();
    toCart($(this).data('id'));
    return false;
});

// Собрать значения фильтров, загрузить товары с сервера
function loadData(page_changed){
    if (!page_changed)
        page_number = 1;
    var filters = new Object();
    var price_range = $('#price-filter .slider').slider( "option", "values" );
    if (price_range.length!==0){
        filters.price_from = price_range[0];
        filters.price_to = price_range[1];
    }
   
    var brands_elem = $('#brands-filter .selected');
    if (brands_elem.length!==0){
        var brands_ids = [];
        brands_elem.each(function(index){
            brands_ids.push($(this).data('value'));
        });
        filters.brands = $.toJSON(brands_ids);
    }
    
    var lens_wearing_time_elem = $('#wearing-time-filter .list .selected');
    if (lens_wearing_time_elem.length!==0)
        filters.lens_wearing_time_id = lens_wearing_time_elem.data('value');
    
    var lens_wearing_mode_elem = $('#wearing-mode-filter .list .selected');
    if (lens_wearing_mode_elem.length!==0)
        filters.lens_wearing_mode_id = lens_wearing_mode_elem.data('value');
    
    var lens_color_elem = $('#lens-color-filter .list .selected');
    if (lens_color_elem.length!==0)
        filters.lens_color_id = lens_color_elem.data('value');
    
    var manufacturers_elem = $('#manufacturers-filter .selected');
    if (manufacturers_elem.length!==0){
        var manufacturers_ids = [];
        manufacturers_elem.each(function(index){
            manufacturers_ids.push($(this).data('value'));
        });
        filters.manufacturers = $.toJSON(manufacturers_ids);
    }
    
    // новинки
    var new_elem = $('#new-filters.pressed');
    if (new_elem.length!==0){
        filters.new = 1;
    }
    
    var sortby_elem = $('#sortby-filter .list .selected');
    if (sortby_elem.length!==0)
        filters.sortby = sortby_elem.data('value');

    // подтипы
    var subtype_elem = $('.subtype-buttons .pressed');
    if (subtype_elem.length!==0){
        var subtype_ids = [];
        subtype_elem.each(function(index){
            subtype_ids.push($(this).data('value'));
        });
        filters.subtypes = $.toJSON(subtype_ids);
    }
    
    filters.page = page_number;
    
    $.ajax({
        type: "get",
        url: "/lenses/getlist/",
        dataType: "json",
        data: filters,
        beforeSend: function(){
        },
        complete: function(){
        },
        success: function(response){
            $('.content').html(response.html);
            count = response.count;
            fill_paginator();
        }
    });
}

