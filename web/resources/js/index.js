/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Клик по товару в блоке "новинки"
$('#novelty .item').on('click', function(){
    switch($(this).data('type')){
        case 1:
            document.location.href = "/sunglasses/product/?id="+$(this).data('id');
            break;
        case 2:
            document.location.href = "/frames/product/?id="+$(this).data('id');
            break;
        case 3:
            document.location.href = "/lenses/product/?id="+$(this).data('id');
            break;
        case 4:
            document.location.href = "/related/product/?id="+$(this).data('id');
            break;
    }
});