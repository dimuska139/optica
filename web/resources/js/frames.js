var page_size = 30; // количество элементов на странице
var page_number = 1;
var count = 0;


$('.content').on('click', '.item', function(){
    document.location.href = "frames/product/?id="+$(this).data('id');
});

$('#clear-filters').on('click', function(){
    location.reload();
});

// Добавление в корзину
$('.content').on('click', '.basket', function(){
    $(this).hide();
    $(this).parent('.bottom').find('.in_cart').show();
    toCart($(this).data('id'));
    return false;
});

// Собрать значения фильтров и загрузить данные с сервера
function loadData(page_changed){
    // Если фильтры изменились, то следует грузить первую страницу
    if (!page_changed)
        page_number = 1;
    var filters = new Object();
    var price_elem = $('#price-filter .list .selected');
    if (price_elem.length!==0){
        filters.price_from = price_elem.data('min');
        filters.price_to = price_elem.data('max');
    }
    var frame_color_elem = $('#frame-color-filter .list .selected');
    if (frame_color_elem.length!==0)
        filters.frame_color_id = frame_color_elem.data('value');
    
  /*  var frame_size_elem = $('#frame-size-filter .list .selected');
    if (frame_size_elem.length!==0)
        filters.frame_size_id = frame_size_elem.data('value');*/
    
    // бренды
    var brands_elem = $('#brands-filter .selected');
    if (brands_elem.length!==0){
        var brands_ids = [];
        brands_elem.each(function(index){
            brands_ids.push($(this).data('value'));
        });
        filters.brands = $.toJSON(brands_ids);
    }
    // формы
    var forms_elem = $('#forms-filter .selected');
    if (forms_elem.length!==0){
        var forms_ids = [];
        forms_elem.each(function(index){
            forms_ids.push($(this).data('value'));
        });
        filters.forms = $.toJSON(forms_ids);
    }
    // оправы
    var frames_elem = $('#frame-filter .selected');
    if (frames_elem.length!==0){
        var frames_ids = [];
        frames_elem.each(function(index){
            frames_ids.push($(this).data('value'));
        });
        filters.frames = $.toJSON(frames_ids);
    }
    // материалы
    var materials_elem = $('#material-filter .selected');
    if (materials_elem.length!==0){
        var materials_ids = [];
        materials_elem.each(function(index){
            materials_ids.push($(this).data('value'));
        });
        filters.materials = $.toJSON(materials_ids);
    }
    
    var sortby_elem = $('#sortby-filter .list .selected');
    if (sortby_elem.length!==0)
        filters.sortby = sortby_elem.data('value');

    // мужские, женские или детские
    var subtype_elem = $('.subtype-buttons .pressed');
    if (subtype_elem.length!==0){
        var subtype_ids = [];
        subtype_elem.each(function(index){
            subtype_ids.push($(this).data('value'));
        });
        filters.subtypes = $.toJSON(subtype_ids);
    }
    
    
    // новинки
    var new_elem = $('#new-filters.pressed');
    if (new_elem.length!==0){
        filters.new = 1;
    }
    
    
    filters.page = page_number;
    
    $.ajax({
        type: "get",
        url: "/frames/getlist/",
        dataType: "json",
        data: filters,
        beforeSend: function(){
        },
        complete: function(){
        },
        success: function(response){
            $('.content').html(response.html);
            count = response.count;
            fill_paginator();
        }
    });
}

loadData();