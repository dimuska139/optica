/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// Инициализация "Pluso" - сервиса для социальных кнопок
(function() {
if (window.pluso)if (typeof window.pluso.start == "function") return;
if (window.ifpluso==undefined) { window.ifpluso = 1;
  var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
  s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
  s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
  var h=d[g]('body')[0];
  h.appendChild(s);
}})();

// Переключение изображений при клике на миниатюру (та, на которую кликнули, становится основной)
$('.another img').on('click', function(){
    var parent_container = $(this).parent('div');
    var temp_url = $('.main img').attr('src');
    $('.main img').attr('src', $(this).attr('src'));
    $(this).attr('src',temp_url);
});
// Клик по товару в блоке "похожие товары"
$('.similar-item, .history-item').on('click', function(){
    switch($(this).data('type')){
        case 1:
            document.location.href = "/sunglasses/product/?id="+$(this).data('id');
            break;
        case 2:
            document.location.href = "/frames/product/?id="+$(this).data('id');
            break;
        case 3:
            document.location.href = "/lenses/product/?id="+$(this).data('id');
            break;
        case 4:
            document.location.href = "/related/product/?id="+$(this).data('id');
            break;
    }
});
// Добавление в корзину
$('#add-to-basket').on('click', function(){
    toCart($(this).data('id'));
    document.location.href = "/cart";
});
// Закрыть всплывающее окно с предложением купить линзы для оправы
$('.popup .close-button, .popup .only-frame').on('click', function(){
    $('.popup').fadeOut();
});