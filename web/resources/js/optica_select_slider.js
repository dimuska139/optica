(function( $ ){
// Фильтр для выбора цены в виде слайдера
    var methods = {
        init : function(options) {
            return this.each(function(){
                var me = this;
                this.state = 0;
                this.animation_speed = 'fast';
                $(this).children('.slider-place').children('.from').html(options.min+' руб');
                $(this).children('.slider-place').children('.to').html(options.max+' руб');
            //    $(this).children('.header').children('.text').html(options.values[0]+' руб - '+options.values[1]+' руб');
                $(this).find('.slider').slider({
                    range: true,
                    animate: true,
                    min: options.min,
                    max: options.max,
                    values: [options.values[0], options.values[1]],
                    change: function(event, ui){
                        $(this).parent('.slider-place').parent('.optica_select_slider').children('.header').children('.text').html(ui.values[0]+' руб - '+ui.values[1]+' руб');
                    },
                    stop: function( event, ui ) {
                        loadData();
                    }
                });
                $(this).on('click',function(event){
                    var list_elem = $(this).children('.slider-place');
                    if ($(event.target).hasClass('text')
                            || $(event.target).hasClass('arrow-place')
                            || $(event.target).hasClass('arrow')){
                        if (this.state==0){
                            list_elem.slideDown(this.animation_speed);
                            this.state = 1;
                            $(this).find('.header img').rotate({animateTo:180});
                        } else {
                            list_elem.slideUp(this.animation_speed);
                            this.state = 0;
                            $(this).find('.header img').rotate({animateTo:0});
                        }
                    }
                /*    var value = event.target.dataset.value;
                    if (typeof value === 'undefined')
                        value = $(event.target).parent('div').data('value');
                    $(this).data('selected', value);
                    var selected_elem = $(this).children('[data-value="'+value+'"]');
                    if (selected_elem.hasClass('selected'))
                        selected_elem.removeClass('selected')
                    else
                        selected_elem.addClass('selected');*/
                });
            });
        }
    };
  
    $.fn.optica_select_slider = function( method ) {
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.optica_select' );
        }
    };
})( jQuery );