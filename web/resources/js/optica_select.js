(function( $ ){
// Фильтр в виде выпадающего списка. Аналог элемента select
    var methods = {
        init : function(options) {
            return this.each(function(){
                this.state = 0;
                this.animation_speed = 'fast';
                this.val = false;
                var selected_by_default = $(this).find('.list .selected');
                if (selected_by_default.length==0)
                    $(this).data('selected', 'false');
                else {
                    var list_elem = $(this).children('.list');
                    var val = selected_by_default.data('selected');
                    this.val = val;
                    $(this).data('selected', val);
                    list_elem.children('div').removeClass('selected');
                
                    $(this).find('.header .text').html(selected_by_default.html());
                    
                    selected_by_default.addClass('selected');
                }
                $(this).on('click',function(event){
                    if ('value' in event.target.dataset){
                        var text_to_header = $(this).find('.list [data-value="'+event.target.dataset.value+'"]').html();
                        this.val = event.target.dataset.value;
                        $(this).data('selected', event.target.dataset.value);
                        $(this).find('.header .text').html(text_to_header);
                        
                        var list_elem = $(this).children('.list');
                        var selected_elem = list_elem.children('[data-value="'+event.target.dataset.value+'"]');
                        list_elem.children('div').removeClass('selected');
                        selected_elem.addClass('selected');
                        loadData();
                    }
                    var list_elem = $(this).children('.list');
                    if (this.state==0){
                        // разворачивание списка
                        list_elem.slideDown(this.animation_speed);
                        this.state = 1;
                        // вращение индикатора состояния
                        $(this).find('.header img').rotate({animateTo:180});
                    } else {
                        // сворачивание списка
                        list_elem.slideUp(this.animation_speed);
                        this.state = 0;
                        // вращение индикатора состояния
                        $(this).find('.header img').rotate({animateTo:0});
                    }
                });
            });
        },
        val: function(val){
       //     return this.each(function(){
            /*    if (typeof val === 'undefined'){
                    return this.val;
                } else {*/
                    var list_elem = $(this).children('.list');
                    var selected_elem = list_elem.children('[data-value="'+val+'"]');
                    if (selected_elem.length>0){
                        this.val = val;
                        $(this).data('selected', val);
                        list_elem.children('div').removeClass('selected');
                        selected_elem.addClass('selected');
                        $(this).find('.header .text').html(selected_elem.html());
                    }
          //      }
    //        });
        }
    };
  
    $.fn.optica_select = function( method ) {
        if ( methods[method] ) {
            return methods[method].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Метод с именем ' +  method + ' не существует для jQuery.optica_select' );
        }
    };
})( jQuery );


/*$('.optica_select').optica_select();
console.dir($('.optica_select').data('selected'));*/