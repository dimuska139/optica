/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


var page_size = 30; // количество элементов на странице
var page_number = 1;
var count = 0;

$('.content').on('click', '.item', function(){
    switch($(this).data('type')){
        case 1:
            document.location.href = "sunglasses/product/?id="+$(this).data('id');
            break;
        case 2:
            document.location.href = "frames/product/?id="+$(this).data('id');
            break;
        case 3:
        //    alert("Карточка товара 'контактные линзы' находится в разработке. Приносим извинения за временные неудобства");
            document.location.href = "lenses/product/?id="+$(this).data('id');
            break;
        case 4:
        //    alert("Карточка товара 'сопутствующие товары' находится в разработке. Приносим извинения за временные неудобства");
            document.location.href = "related/product/?id="+$(this).data('id');
            break;
    }
});

$('#clear-filters').on('click', function(){
    document.location.href = document.location.pathname;
});

// Добавление в корзину
$('.content').on('click', '.basket', function(){
    $(this).hide();
    $(this).parent('.bottom').find('.in_cart').show();
    toCart($(this).data('id'));
    return false;
});

function loadData(page_changed){
    if (!page_changed)
        page_number = 1;
    var filters = new Object();
    var price_elem = $('#price-filter .list .selected');
    if (price_elem.length!==0){
        filters.price_from = price_elem.data('min');
        filters.price_to = price_elem.data('max');
    }
    var frame_color_elem = $('#frame-color-filter .list .selected');
    if (frame_color_elem.length!==0)
        filters.frame_color_id = frame_color_elem.data('value');
    
    var lens_color_elem = $('#lens-color-filter .list .selected');
    if (lens_color_elem.length!==0)
        filters.lens_color_id = lens_color_elem.data('value');
    
    var brands_elem = $('#brands-filter .selected');
    if (brands_elem.length!==0){
        var brands_ids = [];
        brands_elem.each(function(index){
            brands_ids.push($(this).data('value'));
        });
        filters.brands = $.toJSON(brands_ids);
    }
    
    var forms_elem = $('#forms-filter .selected');
    if (forms_elem.length!==0){
        var forms_ids = [];
        forms_elem.each(function(index){
            forms_ids.push($(this).data('value'));
        });
        filters.forms = $.toJSON(forms_ids);
    }
    
    var frames_elem = $('#frame-filter .selected');
    if (frames_elem.length!==0){
        var frames_ids = [];
        frames_elem.each(function(index){
            frames_ids.push($(this).data('value'));
        });
        filters.frames = $.toJSON(frames_ids);
    }
    
    var materials_elem = $('#material-filter .selected');
    if (materials_elem.length!==0){
        var materials_ids = [];
        materials_elem.each(function(index){
            materials_ids.push($(this).data('value'));
        });
        filters.materials = $.toJSON(materials_ids);
    }
    
    var sortby_elem = $('#sortby-filter .list .selected');
    if (sortby_elem.length!==0)
        filters.sortby = sortby_elem.data('value');

    // мужские, женские или детские
    var subtype_elem = $('.subtype-buttons .pressed');
    if (subtype_elem.length!==0){
        var subtype_ids = [];
        subtype_elem.each(function(index){
            subtype_ids.push($(this).data('value'));
        });
        filters.subtypes = $.toJSON(subtype_ids);
    }
    
    // типы товаров
    var good_types_elem = $('#good-types .selected');
    if (good_types_elem.length!==0){
        var goodtype_ids = [];
        good_types_elem.each(function(index){
            goodtype_ids.push($(this).data('value'));
        });
        filters.good_types = $.toJSON(goodtype_ids);
    }
    
    if ($('#search input').val().length!==0){
        filters.search = $('#search input').val();
    }
    
    filters.page = page_number;
    
    $.ajax({
        type: "get",
        url: "/search/getlist/",
        dataType: "json",
        data: filters,
        beforeSend: function(){
        },
        complete: function(){
        },
        success: function(response){
            $('.content').html(response.html);
            count = response.count;
            fill_paginator();
        }
    });
}

loadData();